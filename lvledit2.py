import copy
import os
import traceback
import collections
import functools
import levels
import gameobjs
import main
import inspect
import importlib

import pygame
import pygame.locals as pl
import pyconsolegraphics as pcg
from pyconsolegraphics.ezmode import EZMode
import sys

_framerate=15
_helptext="""
Keyboard:

F1       : Toggle displaying this text
Spacebar : Place at cursor
Backspc. : Delete at cursor
S        : Print the level's code to stdout
R        : Toggle running the level
X        : Reload gameobjs.py
L        : Load a given level
M        : Edit level metadata
Shift    : Toggle object under cursor being visible to in-game console
Ctrl     : Open object properties menu.
[ and ]  : Select object to place
Arrowkeys: Move cursor
Escape   : Quit

Mouse:

Cursor is placed at mouse cursor
Left-Click  : Place at cursor
Right-click : Delete at cursor
Mousewheel  : Select object to place
Mousewheel-click : Open/dismiss menu

"""

#Maps GameObject subclasses to graphics strings, to be used when get_graphic() doesn't work on a certain class.
#(actually I just copied the dict from the old level editor but whatever)
graphic_overrides={gameobjs.Player: "@",
                   gameobjs.Wall: "#",
                   gameobjs.Gate: "█",
                   gameobjs.Levelporter: "⌂",
                   gameobjs.PressurePlate: "♁",
                   gameobjs.Boulder: "○",
                   gameobjs.GravPad: "♨",
                   gameobjs.SafePressurePlate: "♁",
                   gameobjs.AttributeEnforcer: "☑",
                   gameobjs.Teleporter: "˽",
                   gameobjs.Drone: "♙",
                   gameobjs.CombatAndroid: "©",
                   gameobjs.LaserTurret: "⌀",
                   gameobjs.ProtonBeamEmitter: "¥",
                   gameobjs.ProtonBeam: "⁓",
                   gameobjs.ArbitraryCodeExecutor: "␍",
                   gameobjs.ProtonBeamReceiver: "◘"
                   }

def get_graphic(theclass):
    "Return the character that represents the given GameObj."
    if theclass in graphic_overrides:
        return graphic_overrides[theclass]

    try:
        newobj=theclass(startpos=(0,0))
        return newobj.graphic
    except Exception:
        pass

    return "?"

def find_subclasses(module, sprclass):
    "Return all classes defined in a given module that are subclasses of the given class. Written by Runeh of StackOverflow. Thanks!"
    return [cls for name, cls in inspect.getmembers(module)
                if inspect.isclass(cls) and issubclass(cls, sprclass) and cls is not sprclass]

def get_args(func,includeoptional=False):
    """Return a list of the names of a functions required args (args with no default), unless includeoptional is True in which case optimal arguments are included too.
    Written by Brian of StackOverflow. Thanks!"""
    args, varargs, varkw, defaults = inspect.getargspec(func)
    if defaults and not includeoptional:
        args=args[:-len(defaults)]
    if args[0]=="self":
        args=args[1:]
    return args   # *args and **kwargs are not required, so ignore them.

#@TODO: Make this and mousehandler rebindable dispatch dicts on EditorState.
def keyhandler(event,editorstate):
    "Translate keyboard input into editor actions"
    if event.key==pl.K_ESCAPE:
        if editorstate.showconfirmquittimer:
            pygame.event.post(pygame.event.Event(pl.QUIT))
        else:
            editorstate.showconfirmquittimer=_framerate * 5

    elif event.key==pl.K_RIGHTBRACKET:
        editorstate.increment_placement_class(1,0)

    elif event.key==pl.K_LEFTBRACKET:
        editorstate.increment_placement_class(-1,-1)

    elif event.key==pl.K_LEFT:
        x,y=editorstate.cursorpos
        editorstate.cursorpos=(x-1,y)

    elif event.key==pl.K_RIGHT:
        x,y=editorstate.cursorpos
        editorstate.cursorpos=(x+1,y)

    elif event.key==pl.K_UP:
        x,y=editorstate.cursorpos
        editorstate.cursorpos=(x,y-1)

    elif event.key==pl.K_DOWN:
        x,y=editorstate.cursorpos
        editorstate.cursorpos=(x,y+1)

    elif event.key==pl.K_F1:
        editorstate.showhelp=not editorstate.showhelp

    elif event.key==pl.K_RETURN or event.key==pl.K_SPACE or event.key==pl.K_KP_ENTER:
        editorstate.place()

    elif event.key==pl.K_BACKSPACE:
        editorstate.remove()

    elif event.key==pl.K_RSHIFT or event.key==pl.K_LSHIFT:
        editorstate.toggle_visible()

    elif event.key==pl.K_LCTRL or event.key==pl.K_RCTRL:
        editorstate.object_properties_menu()

    elif event.key==pl.K_l:
        editorstate.load()

    elif event.key==pl.K_x:
        importlib.reload(gameobjs)

    elif event.key==pl.K_m:
        editorstate.open_metadata_menu()

    elif event.key==pl.K_s:
        editorstate.print_level()

    elif event.key==pl.K_r:
        editorstate.runtoggle()


def mousehandler(event,editorstate):
    "Translate mouse input to editor actions"
    if event.button==1:
        #Left-click
        editorstate.place()

    elif event.button==3:
        #Right-click
        editorstate.remove()

    elif event.button==4:
        #Mousewheel up
        editorstate.increment_placement_class(1,0)

    elif event.button==5:
        #Mousewheel down
        editorstate.increment_placement_class(-1,-1)

class Menu(collections.OrderedDict):
    """Implements a selectable menu of options, given names and callbacks. The menu items can then be selected with
    keyboard or mouse. Acts as a dict mapping labels to callbacks."""

    def __init__(self,*args,repeat=False):
        super().__init__(*args)
        self.selection=None
        self.itemys={}
        self.repeat=repeat
        self.clock=pygame.time.Clock()

    def option(self,name):
        """Intended to be used as a decorator. Use like this:
        @mymenu.option("Engage self-destruct!")
        def selfdestruct():
            ..."""
        def inner(func):
            self[name]=func
        return inner

    def increment_selection(self):
        "Move the selector down one option. Wraps around to the first option."
        optionlist=list(self.keys())
        idx=optionlist.index(self.selection)
        if idx+1 >= len(self):
            self.selection = optionlist[0]
        else:
            self.selection=optionlist[idx+1]

    def decrement_selection(self):
        "Move the selector up one option. Wraps around to the last option."
        optionlist=list(self.keys())
        idx=optionlist.index(self.selection)
        if idx-1 < 0:
            self.selection=optionlist[-1]
        else:
            self.selection=optionlist[idx-1]

    def draw(self,window:pcg.ezmode.EZMode):
        self.clock.tick(15)
        window.put_cursor((5,5))
        window.blank()
        for y, theoption in enumerate(self.keys()):
            if theoption==self.selection:
                bgcolor="white"
                fgcolor="black"
                x=6
            else:
                bgcolor=fgcolor=None
                x = 5

            window.put_line_at(theoption, (x, y + 5), fgcolor=fgcolor, bgcolor=bgcolor)
            self.itemys[y + 5]=theoption
        window.update()

    def select(self,**kwargs):
        """If the currently selected option is callable, call it and return its return value. Otherwise just return it."""
        selected=self[self.selection]
        #Quick hack to make input()-ing functions not immediately register an ENTER.
        pygame.event.clear()
        if callable(selected):
            return selected(**kwargs)
        else:
            return selected

    def __call__(self,window,defaultselection=None,**kwargs):
        """Make this menu active and allow it to intercept all user input. Does not exit until the menu is exited by
        pressing escape. If the object mapped to the selected option is callable, call it and return its return value;
        otherwise, return it. Any given keyword arguments are passed into contained callables."""
        if len(self) == 0:
            raise ValueError("Tried to activate an empty menu")

        self.selection=defaultselection
        if self.selection is None:
            self.selection=list(self.keys())[0]
        pygame.key.set_repeat(100,50)
        pygame.mouse.set_visible(True)
        try:
            while True:

                self.draw(window)

                for event in pygame.event.get():
                    if event.type==pl.QUIT:
                        pygame.event.post(pl.QUIT)
                        return

                    elif event.type==pl.KEYDOWN:
                        if event.key==pl.K_ESCAPE:
                            return

                        elif event.key==pl.K_DOWN:
                            self.increment_selection()

                        elif event.key==pl.K_UP:
                            self.decrement_selection()

                        elif event.key in (pl.K_KP_ENTER,pl.K_RETURN,pl.K_SPACE):
                            result=self.select(**kwargs)
                            if not self.repeat:
                                return result

                    elif event.type==pl.MOUSEMOTION:
                        x, y=window.terminal.backend.px_to_cell(event.pos)
                        if y in self.itemys:
                            self.selection=self.itemys[y]

                    elif event.type==pl.MOUSEBUTTONDOWN:
                        x,y=window.terminal.backend.px_to_cell(event.pos)
                        if y in self.itemys:
                            self.selection=self.itemys[y]
                        result=self.select(**kwargs)
                        if not self.repeat:
                            return result
        finally:
            pygame.mouse.set_visible(False)


class EditorState(main.GameState):
    "Singleton that holds the level editor's global state."

    def __init__(self):

        if sys.platform == "win32":
            os.environ["SDL_VIDEODRIVER"] = "directx"

        term = pcg.Terminal((80, 30), main._font_path, 16)
        term.stdiocursor = pcg.InputCursor(term, cursorchar="")
        mainwin = EZMode(term)
        mainwin._cursor_predicate()
        mainwin.terminal.ezmodecursor.cursorchar = ""

        super().__init__(mainwin)
        self.current_level_name="Untitled"
        self.current_level_id="level_"
        self.current_level=levels.Level("Untitled",[],gameobjs.Player((-1,-1)))
        self.current_level_playlist="Calm"
        self.gameobjlist=find_subclasses(gameobjs,gameobjs.GameObj)
        self.placement_class=self.gameobjlist[0] #The class of the object that the cursor is currently set to place
        self.cursorpos=(0,0)
        self.showhelp=False
        self.showconfirmquittimer=0
        self.namedobjs={}
        self.origlevel=None
        self.metadatamenu=Menu({"Set level name": self.set_level_name,
                                "Set level ID": self.set_level_machine_name,
                                "Set level music": self.set_level_playlist},
                               repeat=True)

    def run(self):
        "Start playing the level as if it was loaded in the actual game."
        if not self.current_level.active:
            self.origlevel=self.current_level
            playlevel=copy.deepcopy(self.current_level)
            self.current_level=playlevel
            self.current_level.active=True

    def stop(self):
        if self.origlevel:
            self.current_level=self.origlevel
            self.origlevel=None

    def runtoggle(self):
        "Call run() if the level is inactive, stop() if it's active."
        if self.current_level.active:
            self.stop()
        else:
            self.run()

    def update(self):
        if self.current_level.active:
            super().update()

    def draw(self):

        self.mainwin.put_line_at(self.placement_class.__name__,(0,0),fgcolor="gray")
        self.mainwin.put_line_at(str(tuple(self.cursorpos)),(71,0),fgcolor="gray")
        self.mainwin.put_line_at(get_graphic(self.placement_class),self.cursorpos,fgcolor="gray")

        #We put a light highlight on objects that are exposed to the console
        for theobj in self.current_level.objlist:
            try:
                self.mainwin.terminal[theobj.pos].bgcolor = (64,64,64)
            except IndexError:
                pass

        super().draw()

        if self.showhelp:
            self.mainwin.put_line_at(_helptext,(71,2))
        else:
            self.mainwin.put_line_at("F1 for help",(35,0),fgcolor="gray")

        if self.showconfirmquittimer:
            self.mainwin.put_line_at("Pres ESC again to quit",(18,1),fgcolor="red")
            self.showconfirmquittimer-=1

        self.mainwin.update()

    def increment_placement_class(self,by,rollover):
        """Set editorstate.placement_class to gameobjlist[k+by], where k is the current value's index."""
        try:
            idx=self.gameobjlist.index(self.placement_class)
            self.placement_class=self.gameobjlist[idx+by]
        except IndexError:
            #If we went off the end, wrap back around to the first item in the list
            self.placement_class=self.gameobjlist[rollover]

    def place(self,cls=None,pos=None,**kwargs):
        """Put a cls at pos, requesting additional information from the user as needed. Cls defaults to .placement_class,
        pos defaults to .cursorpos."""

        if cls is None:
            cls=self.placement_class
        if pos is None:
            pos=self.cursorpos

        #Players are handled specially... kinda just 'cos.
        #Mainly because having more than one makes no sense.
        if cls is gameobjs.Player:
            self.current_level.plyobj.pos=pos
            return

        #You're really not supposed to have more than one object on the same square except in special cases
        #so we delete whatever's already there first. If you want to have a boulder start on a pplate or something
        #just modify the generated level code yourself.
        self.remove(pos)

        initargs=kwargs.copy()
        initargs["startpos"]=pos

        argsneeded=[thearg for thearg in get_args(cls) if thearg not in initargs]

        #We just grab extra info we need straight from the user via text prompts
        for thearg in argsneeded:
            self.mainwin.put_cursor((5,5))
            newargtext=self.mainwin.get_line("{0}: ".format(thearg),fgcolor="black",bgcolor="white")
            try:
                newargvalue=eval(newargtext,{},self.namedobjs)
            except:
                self.mainwin.put_line(traceback.format_exception_only(*sys.exc_info()[:2])[0])
                self.mainwin.put_line("Input interpreted as string. [Press ENTER to continue]")
                newargvalue=str(newargtext)
                self.mainwin.update()
                self.mainwin.wait()
            initargs[thearg]=newargvalue

        objnames={obj:name for name,obj in self.namedobjs.items()}

        try:
            theobj=cls(**initargs)
            argstrings=["{0}={1}".format(key,repr(value) if value not in objnames else objnames[value]) for key,value in initargs.items()]
            theobj.creationstring="gameobjs.{0}({1})".format(type(theobj).__name__,", ".join(argstrings))
        except Exception:
            self.mainwin.put_line(traceback.format_exception_only(*sys.exc_info()[:2])[0])
            self.mainwin.put_line("Object not placed.")
            return
        else:
            self.current_level.safe_objlist.append(theobj)

    def remove(self,pos=None):
        "Delete the game object at the given position. Pos defaults to the editor's cursor position."
        if pos is None:
            pos=self.cursorpos

        obj=self.current_level.get_at(pos, include_player=True)

        #The player object is a special case here too - you're not supposed to remove_obj it, for one thing.
        if isinstance(obj,gameobjs.Player):
            self.current_level.plyobj.pos=(-1,-1)
            return

        self.current_level.remove_obj(obj)

    def toggle_visible(self,pos=None):
        """If it isn't already, move the object at the cursor from safe_objlist to the normal objlist, making it
        available to the injection console in-game. If it's already visible, perform the inverse of that operation."""

        if pos is None:
            pos=self.cursorpos

        obj=self.current_level.get_at(pos)

        if not obj:
            return

        #Again, player objects are special - it isn't in EITHER list!
        if isinstance(obj,gameobjs.Player):
            return

        if obj in self.current_level.safe_objlist:
            self.current_level.safe_objlist.remove(obj)
            self.current_level.objlist.append(obj)
        elif obj in self.current_level.objlist:
            self.current_level.objlist.remove(obj)
            self.current_level.safe_objlist.append(obj)

    def load(self,levelname=None):
        """Given a level name, switch to it (or display an error if the name is bad). If no levelname is given, requests
        one from the user."""

        if levelname is None:
            levelname=self.mainwin.get_line("Level name: ", promptpos="center")

        try:
            self.change_level(levelname)
        except KeyError:
            self.mainwin.put_line("Bad level name.")

    def open_metadata_menu(self):
        self.metadatamenu(self.mainwin)

    def set_level_name(self,newname=None):
        if newname is None:
            newname=self.mainwin.get_line("Level name: ", promptpos="center")
        self.current_level_name=newname

    def set_level_machine_name(self,newid=None):
        if newid is None:
            newid=self.mainwin.get_line("Level ID: ", promptpos="center")
        self.current_level_id=newid

    def set_level_playlist(self,plistname=None):
        if plistname is None:
            plistselector=Menu({"Calm":"Calm","Intense":"Intense","Super-Intense":"Super-Intense"})
            plistname=plistselector(self.mainwin,defaultselection=self.current_level_playlist)

        self.current_level_playlist=plistname

    def gen_level_string(self):
        "Return a string containing the Python code to build the current level."
        strs=[]

        strs.append("{0}=Level(\"{1}\",[],gameobjs.Player({2}),playlists[\"{3}\"])".format(self.current_level_id,
                                                                                           self.current_level_name,
                                                                                           self.current_level.plyobj.pos,
                                                                                           self.current_level_playlist))

        objnames={}
        for thename, theobj in self.namedobjs.items():
            objnames[theobj]=thename
            strs.append("{0}={1}".format(thename,theobj.creationstring))

        objsstrlist=[]
        for theobj in self.current_level.objlist:
            if isinstance(theobj,gameobjs.Player):
                continue
            if theobj in objnames:
                objsstrlist.append(objnames[theobj])
            else:
                objsstrlist.append(theobj.creationstring)
        strs.append(("{0}.objlist=["+',\n                   '.join(objsstrlist)+"]").format(self.current_level_id))

        safeobjsstrlist=[]
        for theobj in self.current_level.safe_objlist:
            if isinstance(theobj,gameobjs.Player):
                continue
            if theobj in objnames:
                safeobjsstrlist.append(objnames[theobj])
            else:
                safeobjsstrlist.append(theobj.creationstring)
        strs.append(("{0}.safe_objlist=["+',\n                   '.join(safeobjsstrlist)+"]").format(self.current_level_id))

        return "\n".join(strs)

    def print_level(self,file=None):
        if file is None:
            file=sys.stdout
        print(self.gen_level_string(),file=file)

    def give_name(self,theobj,name=None):
        """Make the given gameobj a 'named object' that can be referred to by other objects. What this actually means is
        that the object will be assigned to a variable right after the Level() declaration and that variable will be used
        in its place in the objlist/safe_objlist declaration."""

        for objname, obj in self.namedobjs.items():
            if obj is theobj:
                oldname=objname
                removeorleave="remove"
                break
        else:
            oldname=None
            removeorleave="leave without"

        if not name:
            newname=self.mainwin.get_line("Name (blank to {0} name): ".format(removeorleave), promptpos="centerleft")

            if oldname:
                del self.namedobjs[oldname]
            if not newname:
                return
            name=newname

        if type(theobj) == dict:
            print("Oh fuck! I almost tried to put a dict in self.namedobjs[]!")
            return
        self.namedobjs[name]=theobj

    def object_properties_menu(self):
        "Open the object properties menu on the object under the cursor."
        theobj=self.current_level.get_at(self.cursorpos)

        if not theobj:
            return

        attribs=get_args(type(theobj),includeoptional=True)
        if not attribs:
            return

        def setter(attrname):
            try:
                valuestr=self.mainwin.get_line("{0} = ".format(attrname), promptpos="center")
                value=eval(valuestr,globals(),self.namedobjs)
                setattr(theobj,attrname,value)
                thecstr=theobj.creationstring
                try:
                    #This only works if the property is already there
                    terms=eval("collections.OrderedDict"+thecstr[thecstr.find("("):])
                    print(terms)
                    terms[attrname]=value
                    #You know, back when I started the level editor refactoring project I wanted to make the new code
                    #consistently clean and efficient through out the entire script...
                    newcstr=type(theobj).__name__+"("+", ".join(["{0}={1}".format(param,
                                                                                  repr(value) if value not in self.namedobjs
                                                                                  else {v:k for k,v in self.namedobjs}[value])
                                                                 for param, value in terms.items()])+")"
                    theobj.creationstring=newcstr
                    print(newcstr)
                except KeyError:
                    #If it's not, we just stick it on the end.
                    cstrwoclosingparen=theobj.creationstring[:-1]
                    newcstr=cstrwoclosingparen+"{0}={1}".format(attrname,valuestr)
                    if cstrwoclosingparen[-1]!="(": #If the first character before the closing paren is the opening paren then we don't need a comma
                        newcstr=newcstr+","
                    theobj.creationstring=newcstr+")"
                    print(theobj.creationstring,file=sys.__stdout__)
            except Exception:
                self.mainwin.put_line(traceback.format_exception_only(*sys.exc_info()[:2])[0])
                self.mainwin.update()
                self.mainwin.wait()

            return True

        while True:
            attribdict={attrname+" == "+repr(getattr(theobj,attrname)): functools.partial(setter,attrname) for attrname in attribs if hasattr(theobj,attrname)}

            for objname,obj in self.namedobjs.items():
                if obj is theobj:
                    nameopslabel="Change/delete name (is {0})".format(objname)
                    break
            else:
                nameopslabel="Give name"

            attribdict[nameopslabel]=functools.partial(self.give_name,theobj)

            if not Menu(attribdict)(self.mainwin):  #Because setter returns true, the menu will return true if something was set, None if not
                break                   #all this hackery is needed so the titles update





def mainloop():
    editorstate=EditorState()
    clock=pygame.time.Clock()
    pygame.mouse.set_visible(False)
    pygame.key.set_repeat(50,20)

    while True:
        for event in pygame.event.get():
            if event.type==pl.QUIT:
                sys.exit()

            elif event.type==pl.KEYDOWN:
                keyhandler(event,editorstate)

            elif event.type==pl.MOUSEBUTTONDOWN:
                mousehandler(event,editorstate)

            elif event.type==pl.MOUSEMOTION:
                editorstate.cursorpos=editorstate.mainwin.terminal.backend.px_to_cell(pygame.mouse.get_pos())


        editorstate.update()
        editorstate.mainwin.blank()
        editorstate.draw()

        clock.tick(_framerate)


if __name__ == '__main__':
    mainloop()

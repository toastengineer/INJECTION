﻿To Play:
Use your Python interpreter to execute main.py. If you get errors about pygcurse, install it with pip.

Your character is the @ symbol. You move it with the arrow keys. Walk into objects to interact with them.
Press ; to bring up the injection console. This is an interactive Python environment with access to
objects in the level. Manipulate the program's innards to get to the teleporter, marked by the ⌂ symbol,
to get to the next level. The level names usually contain hints.

The MIT License (MIT)

Copyright (c) 2015 Schilcote

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


TROUBLESHOOTING:

	Semicolon does not bring up console

If you're on a non-QWERTY keyboard layout, you may encounter problems activating the injection console. This bug may have been fixed,
but if it hasn't, just press keys until one does the job.

	I made the level unwinnable (including by setting objs to something else)

Press R and type "yes" to restart the level. This resets everything, including the injection interpreter state.

	I'd like to record this game with FRAPS or some similar software.
	
INJECTION uses the pygame graphics library, which by default uses a rendering backend that most screen recorders can't intercept.
Starting the game with the parameter "-force_backend directx" will make it use directx instead, which has slightly odd behavior but is
confirmed to work with FRAPS. This setup is not recommended for normal play, however, if only because it isn't as thoroughly tested.

	There's weird delays when saving and/or in between levels!

This is a result of stupid networking code design on my part. I'll fix it eventually. In the meantime, either make sure
you have an internet connection when you're playing or run the game with the -telemetry_disabled switch.

	Where are my save files kept?

On windows: %appdata%\INJECTION\
On Linux: ~/.INJECTION/
On MacOS: ~/Library/Application Support/INJECTION

    Cython isn't compiling fx.pyx for me on Windows!

I'm pretty sure this has something to do with Cython not knowing where to find Numpy, and I don't know how to fix
pyximport so that it knows. Run buildexe.bat instead, then go into the build\lib[something]\ folder and copy the pyd
file from there into the same directory as main.py. If buildexe seems to fail, try tinkering with it so that it points
to where things actually are on your machine, and removing any command line options it complains about.

DRONE API:

One of the most complex challenges you'll have to face in this game is writing AI functions for drone objects.
Drone.think() is passed a special proxy object that allows the player-written function to have a certain amount
of access to the drone object itself. It is passed to the function as its first and only parameter, and its API is as such:

self.move(x,y) : Move the drone in the given direction 
(i.e. move(0,1) moves it down the screen, (1,0) moves it right)
        
self.pos : A 2-tuple containing the drone's position.
self.color : A 3-tuple of RGB values; the drone icon will be this color.
self.bumped : If the drone bumped into something last tick this points to it

Don't forget that you can store your own attributes on the proxy object;
that's the only way to have state that persists across turns.

TIPS 'N TRICKS:

dir() shows you all the things currently in your namespace; dir(obj) shows you all the attributes of the given obj.

Every object the player can access has a __doc__, even functions in the funcs list.

Docstrings look much prettier if you print() them.

Any level that has a drone (the pawn-looking thing, pawn as in the chess piece) is going to have an upload function in its
funcs list.

stdout is redirected to the injection console while level objects are updating; anything that gets printed will be there
next time you bring it up

CREDITS:
Font is Fixedsys Excelsior 3.0 (FSEX300.ttf)
http://www.fixedsysexcelsior.com

Written in Python 3.4: http://python.org
Uses Pygame SDL wrapper: http://www.pygame.org
Uses Pygcurse terminal emulation library: http://inventwithpython.com/pygcurse/
Uses Pyperclip clipboard handling library: http://coffeeghost.net/2010/10/09/pyperclip-a-cross-platform-clipboard-module-for-python/
Uses Jedi Python code autocompletion library: http://jedi.jedidjah.ch/en/latest/
The website linked to by one of the in-game tutorials and license information about such is here: http://learnxinyminutes.com/docs/python3/
Uses Dill extended serializer: http://trac.mystic.cacr.caltech.edu/project/pathos/wiki/dill

Music:
All music attained from http://jamendo.com; licensed under Creative Commons licenses.
Most restrictive license for tracks used is CC BY-NC-SA 4.0: https://creativecommons.org/licenses/by-nc-sa/4.0/
(some are CC BY-SA or just CC BY)
Please see jamendo.com entries for license details.
Paul De Laek - ET24
Arbietshield - Gormandize
Mop Cortex - Bleeding Brain
Arbietshield - Square
Seazo - Flush
DJ Mar S Aka One Milk - Malfunction
Atsub - Afterdark
Lukal - Glitch
Nocturnes & dreamscapes - Reflection
Lalo - Crackle.ar
Brainstem - Badchoice
Brainstem - Mixdown
Brainstem - Infoleak
kiyo - Brown Deepsea Flight
Extrapool - Antherosclerosis at 30
XNDL - extraterrestrisch
ATW - Git 'n Glitch
MUPPETMO - TEMA 9
Noqturne - Science (Hell yeah!)
Peter Saar - CornkroncsF4
Jonas Niemann - Fressmaschinen (Prolog)
Retrospecter - In The Rainforest

'''
Created on Nov 2, 2014

@author: Schilcote

Simplistic graphical level editor. It's not a complete solution; you'll still have to move things to the unprotected gameobjs list
edit parameters of more complex objs, and set up triggers by hand. Still, it's better than doing it all yourself.
'''

import main
import atexit
atexit.unregister(main.final_failsafe_dump)
import levels
import gameobjs
import pygcurse
import pygame
import inspect
from pygame.locals import *
import os.path
import traceback
import sys
import imp

clscharmap= {
             gameobjs.Player : "@",
             gameobjs.Wall : "#",
             gameobjs.Gate : "█",
             gameobjs.Levelporter : "⌂",
             gameobjs.PressurePlate : "♁",
             gameobjs.Boulder : "○",
             gameobjs.GravPad : "♨",
             gameobjs.MissileLauncher : "☸",
             gameobjs.SafePressurePlate : "♁",
             gameobjs.AttributeEnforcer : "☑",
             gameobjs.Teleporter : "˽",
             gameobjs.Drone : "♙",
             gameobjs.CombatAndroid : "©",
             gameobjs.LaserTurret : "⌀",
             gameobjs.ProtonBeamEmitter: "¥",
             gameobjs.ProtonBeam: "⁓",
             gameobjs.ArbitraryCodeExecutor: "␍",
             gameobjs.ProtonBeamReceiver: "◘"
             }

def find_subclasses(module, clazz):
    "Return all classes defined in a given module that are subclasses of the given class. Written by Runeh of StackOverflow. Thanks!"
    return [cls for name, cls in inspect.getmembers(module)
                if inspect.isclass(cls) and issubclass(cls, clazz)]

def required_args(func):
    "Return a list of the names of a functions required args (args with no default). Written by Brian of StackOverflow. Thanks!"
    args, varargs, varkw, defaults = inspect.getargspec(func)
    if defaults:
        args=args[:-len(defaults)]
    if args[0]=="self":
        args=args[1:]
    return args   # *args and **kwargs are not required, so ignore them.

def print_level(level):
    "Print out a level so that it can be copy-n'-pasted into levels.py"

    plypos=(-1,-1)
    for theobj in level.objlist:
        if isinstance(theobj,gameobjs.Player):
            plypos=theobj.pos
            break

    print("level_=Level(\"{0}\",[],gameobjs.Player({1}))".format(level.name,plypos))
    objsstr=',\n                   '.join([theobj.makestr for theobj in level.objlist if not isinstance(theobj,gameobjs.Player)])
    print("level_.safe_objlist=[",objsstr,"]")

if __name__ == '__main__':
    pygscreen=pygcurse.PygcurseWindow(80, 30, "INJECTION Level Editor")
    pygscreen.autoupdate=False
    pygscreen.font=pygame.font.Font("fsex2.ttf", 18)

    clock=pygame.time.Clock()

    lvlobjs=[]
    level=levels.Level("",lvlobjs,gameobjs.Player((-1,-1)))
    fakegamestate=main.GameState(None)
    fakegamestate.current_level=level
    fakegamestate.mainwin=pygscreen
    classes=find_subclasses(gameobjs,gameobjs.GameObj)
    classes.remove(gameobjs.Trigger)
    classes.remove(gameobjs.GameObj)
    currentcls=classes[0]   #This holds the class we're currently "painting" onto the level
    currentclsidx=0 #This holds the index of the class we're "painting" in the classes list
    gameobjsstr=""  #This'll hold the actual code for generating the level's gameobjs list
    runlevel=False  #This toggles whether or not the level updates in the editor
    frame=0

    while True:

        #input
        for theevent in pygame.event.get():
            evtype=theevent.type
            if evtype==QUIT:
                exit()
            elif evtype==MOUSEBUTTONDOWN:
                if theevent.button==1:
                    #L-click: place current gameobj
                    pos=pygscreen.getcoordinatesatpixel(*theevent.pos, onscreen=True)
                    reqargs=required_args(currentcls.__init__)
                    args=[]
                    argtexts=[]
                    pygscreen.cursor=(0,0)
                    #first arg is always pos; we know that already!
                    for thearg in reqargs[1:]:
                        argtext=pygscreen.input(thearg+": ", fgcolor="black", bgcolor="white", promptfgcolor="black", promptbgcolor="white")
                        #if we can evaluate the text, such as if it's "(0,0)", we store the attribute as what it MEANS,
                        #not the text itself. If there's a problem evaluating it, we just assume it's meant as a string.
                        try:
                            args.append(eval(argtext))
                        except Exception:
                            traceback.print_exc(file=sys.stdout)
                            args.append(argtext)
                        argtexts.append(argtext)
                    #Sometimes we can accidentally put in invalid parameters that will cause a crash
                    #so we have a try...except here too
                    try:
                        obj=currentcls(pos,*args)
                        lvlobjs.append(obj)
                    except Exception:
                        traceback.print_exc(file=sys.stdout)
                        continue
                    #And we store a string on the object telling us how to define it
                    args.insert(0, obj.pos)
                    obj.makestr=''.join(["gameobjs.",currentcls.__name__,"(",','.join([repr(obj.pos)]+argtexts),")"])
                elif theevent.button==3:
                    #R-click: Delete obj at cursor
                    pos=pygscreen.getcoordinatesatpixel(*theevent.pos, onscreen=True)
                    level.remove_obj(level.get_at(pos))
                elif theevent.button==4:
                    #Wheel up: Next obj class
                    currentclsidx+=1
                    try:
                        currentcls=classes[currentclsidx]
                    except IndexError:
                        currentclsidx=0
                        currentcls=classes[currentclsidx]
                elif theevent.button==5:
                    #Wheel down: Last obj class
                    currentclsidx-=1
                    try:
                        currentcls=classes[currentclsidx]
                    except IndexError:
                        currentclsidx=0
                        currentcls=classes[currentclsidx]
            elif evtype==KEYDOWN:
                key=theevent.key
                if key==K_s:
                    #"save" (print) the level
                    print_level(level)
                elif key==K_r or key==K_e:
                    #toggles running/updating of the level (level objects operate as if they were in the game proper.
                    runlevel=not runlevel
                    if not runlevel:
                        #if we're stopping the level, we want to reset to the state before we started;
                        #that's what print_level will produce
                        lvlobjs=[eval(obj.makestr) for obj in level.objlist if getattr(obj,"makestr",None)]
                        for k, theobj in enumerate(level.objlist):
                            try:
                                lvlobjs[k].makestr=theobj.makestr
                            except AttributeError:
                                continue
                        level.objlist=lvlobjs
                elif key==K_u:
                    imp.reload(gameobjs)

        #game logic
        if runlevel:
            for theobj in lvlobjs+level.safe_objlist:
                try:
                    theobj.update(fakegamestate)
                except Exception:
                    traceback.print_exc(file=sys.stdout)
                    level.remove_obj(theobj)
        #draw
        pygscreen.fill()
        for theobj in lvlobjs+level.safe_objlist:
            #sometimes malformed objects crash on draw; to stop them from ruining the user's work,
            #we have a try...except that replaces them with a red double exclamation to signify an error
            try:
                theobj.draw(pygscreen)
            except Exception:
                traceback.print_exc(file=sys.stdout)
                pygscreen.putchar("‼", *theobj.pos, fgcolor="red")
        cursorpos=pygscreen.getcoordinatesatpixel(*pygame.mouse.get_pos(), onscreen=True)
        try:
            pygscreen.putchar(clscharmap[currentcls],*cursorpos, fgcolor="gray")
        except KeyError:
            pygscreen.putchar("?",*cursorpos)
        pygscreen.putchars(currentcls.__name__, 0, 0, fgcolor="gray")
        pygscreen.putchars(str(cursorpos), 71, 0, fgcolor="gray")

        pygscreen.update()

        #pygame.image.save(pygscreen.surface, os.path.join("D:\\","movie","frame"+str(frame)+".png"))
        #frame+=1

        clock.tick(30)

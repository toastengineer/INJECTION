import copy
import functools
import inspect
import math
import itertools
import sys
import time
import traceback
import webbrowser

import pyconsolegraphics as pc

import levels
import main
if __name__ == '__main__':
    print("This file is not meant to be executed. Please try main.py.")
    exit()

import pygame
from pygame.math import Vector2
import pygcurse
import random

import fx #NOT THE FX ON PYPI; RECOMPILE FX.PYX WITH Cython

def null_func(*attrs):
    """A function that does absolutely nothing. Used as the default think for drones."""
    pass

def movement_check(newpos,level,source,nonphysical=False):
    """Check if there is a collidable object at newpos. Returns None if it's okay to move; the blocking object if not.
    Also scrambles the color on walls (representing them changing color when they're bumped in to) if nonphysical==False"""
    x,y = newpos
    hit=level.get_at((int(x),int(y)),True, exclude=(source,)+no_collide_objs)

    if hit and hit.collide(source,nonphysical):
        return hit

def distance(a, b):
    """Return the distance between A and B's int_pos() if theyre gameobjs, them as 2-iters if not. If A or B is None, distance is returned as infinite."""
    if not (a and b):
        return float("+inf")

    if isinstance(a, GameObj):
        ax, ay = a.int_pos()
    else:
        ax, ay = a

    if isinstance(b, GameObj):
        bx, by = b.int_pos()
    else:
        bx, by = b

    return math.sqrt((ax-bx)**2+(ay-by)**2)

def raycast(pos, vec, level, ignore_objs=[], include_intangible=False, retpos=False, retlist=False):
    """Find the nearest object to the given position along the given vector tuple and return it. Works in the pygcurse
    coordinate space (it'll look at cells, not pixels, unlike laser()). Try to avoid raycasting in a direction where there
    isn't anything. The direction vector must be normalized; otherwise bad things will happen. You may want to use Pygame's
    built-in vector class. If retpos is true, returns the position of the object hit instead of the object itself.
    If retlist is true, instead returns a list of positions along the raycast. These two flags are mutually exclusive.
    If include_intangible is True, the raycast also hits things like pressureplates and open gates."""
    dx, dy = vec
    t=0
    x, y = pos
    obj=None
    visited=set()
    while t < 100:
        t+=1

        x+=dx
        checkpos=(round(x),round(y))
        visited.add(checkpos)
        obj=level.get_at(checkpos,True)
        if obj and (obj not in ignore_objs) and (include_intangible or movement_check(checkpos,level,None,True)): break

        y+=dy
        checkpos=(round(x),round(y))
        if checkpos not in visited:
            visited.add(checkpos)
            obj=level.get_at((round(x),round(y)),True)
            if obj and (obj not in ignore_objs) and (include_intangible or movement_check(checkpos,level,None,True)): break

    if retpos:
        return (round(x),round(y))
    elif retlist:
        return list(visited)
    else:
        return obj

def raycast_to(src, dest, level, **kwargs):
    """Run a raycast from the src pos to the dest pos and return whatever the raycast hits"""
    newsrc=pygame.math.Vector2(src)
    direction=pygame.math.Vector2(dest)-newsrc
    direction.normalize_ip()
    #newsrc+=direction
    return raycast(newsrc, direction, level, **kwargs)

def get_px_pos(vec,mainwin):
    """Given a 2-tuple of ints or a GameObj, return the on-screen pixel coordinates of the pixel in the center of
    the given cell position or the cell position of the given gameobj. In other words it gives you the pixel coordinates
    of whatever you give it."""
    if isinstance(vec, GameObj): vec = vec.pos
    return mainwin.terminal.backend.cell_to_px(pc.Pos(vec) + pc.Pos(0.5, 0.5))

def laser(srcobjorpos,destobjorpos,gamestate,beamcolor=None):
    """Fire a deadly laser at tgt from src, where both are GameObj-s. This does a pixel-by-pixel raycast in the pygame vector space,
    not pygcurse's. In other words it looks in the direction from the source to the target until it sees a pixel that isn't black.
    It "breaks" the game model, sort of. Returns the GameObj that the laser hit and also creates a Laser() object to represent the laser beam."""
    srcpos  = pc.Pos( getattr(srcobjorpos,  "pos", srcobjorpos)  )
    destpos = pc.Pos( getattr(destobjorpos, "pos", destobjorpos) )

    backend = gamestate.mainwin.terminal.backend
    surf = backend.surface

    srcpx, destpx = backend.cell_to_px( srcpos + (0.5, 0.5) ), backend.cell_to_px( destpos + (0.5, 0.5) )
    direction = (destpx - srcpx).normalized()

    #It'd be boring to have the lasers be perfectly accurate, so we apply a small deviation (+-5 degrees)
    direction = direction.rotate( random.randint(-5, 5) )

    # Skipvec is the vector that represents a line exactly parallel with the line from our source to our target
    # that just fits inside one Pygcurse cell. In other words, it's the distance we need to go to "skip" an entire
    # cell's worth of pixels
    skipvec = direction * abs(pc.Pos(gamestate.mainwin.terminal.backend.cellwidth,
                                     gamestate.mainwin.terminal.backend.cellheight) * direction)

    # We start a little ways out so that we don't hit the thing we're starting inside - moving into the next cell works
    srcpx += skipvec

    black = gamestate.mainwin.terminal.bgcolor

    #I think this is glitched such that it starts checking in the MIDDLE of an occupied cell and not the edge it entered
    #from, but to be honest it works fine anyway.
    surf.lock()
    try:
        checkx, checky = srcpx
        dx,     dy     = direction
        boundx, boundy = surf.get_size()
        while 0 < checkx < boundx and 0 < checky < boundy:
            if not gamestate.current_level.get_at(gamestate.mainwin.terminal.backend.px_to_cell((checkx, checky)), True):
                checkx, checky = (skipvec + (checkx, checky))
                continue

            checkx += dx
            if surf.get_at(( int(checkx), int(checky) )) != black:
                break

            checky += dy
            if surf.get_at(( int(checkx), int(checky) )) != black:
                break

    finally:
        surf.unlock()

    hitpx = ( checkx, checky )
    hitcell = backend.px_to_cell(hitpx)

    if not beamcolor:
        beamcolor=(random.randint(0,255),random.randint(0,255),random.randint(0,255))
    gamestate.current_level.objlist.append(LaserBeam(backend.cell_to_px(srcpos + (0.5, 0.5)), hitpx, beamcolor))

    hit = gamestate.current_level.get_at(hitcell, include_player=True)
    return hit

def clamp(x,maxv=None,minv=None):
    """Make sure x is between max and min. Returns max if x > max, returns min if x < min, else returns x. Either one
    is ignored if omitted."""
    return min(max(x,minv),maxv)

class GameObj:
    """The subclass from which all in-game objects are derived."""

    def __init__(self,graphic=' ',pos=(-1,-1)):
        self.graphic=graphic #The char that is drawn to represent this obj
        self.pos=pos

    def update(self,gamestate):
        """Called each frame. Contains your game logic code.
        :type gamestate main.GameState"""
        pass

    def interact(self,player,gamestate):
        """Called when the player tries to interact with this object (i.e. walks into the same square as it).
        This gets called as part of the player object's movement process, from its update(), so don't worry
        about race conditions or anything.
        :type gamestate main.GameState
        :type player Player"""
        pass

    def trigger(self):
        """Called by a Trigger subclass instance. A touchplate, for example, would call trigger() on its target when
        the player steps on it. A gate, for example, will open when triggered."""
        pass

    def cleanup(self,gamestate):
        """Called by level.remove_obj() when this object is removed. Use it to make any last-minute preparations to be removed from the game.
        :type gamestate main.GameState"""
        pass

    def kill(self,src,gamestate):
        """Called by any other GameObject that wants to destroy this GameObject.
        :type gamestate main.GameState
        :type src GameObj"""
        gamestate.current_level.remove_obj(self)

    def collide(self,other,nonphysical=False):
        """Return True if the given object is NOT allowed to pass through this object.
        Note that if this object is in no_collide_objs it will never receive this message.

        other is the GameObj that wants to pass through. It may be None.

        If nonphysical is True then this _doesn't_ represent an actual impact or touch.
        Raycasts, for example, call movement_check (which calls collide) to see if they can pass through
        a position.

        :type other GameObj"""
        return True

    def draw(self,window,color=None):
        """Called each frame to draw the object's character representation to the screen."""
        #There are certain circumstances where an object will have non-integer pos, such as the boulder
        #having fraction velocity, and I'm not sure how pygcurse deals with that, so we just int-cast
        #window.putchar(self.graphic, int(x), int(y), fgcolor=color)
        try:
            window.terminal[self.pos].character=self.graphic
        except IndexError:
            return
        if color:
            window.terminal[self.pos].fgcolor=color


    def glow(self,color,power,window,origin=None,hard=False,flicker=False):
        """Objects that glow should call this in their draw(). 'Spills' the given color onto nearby uncolored objects.
        Power is an int that controls how far the glow goes and how much coloring closer objects get. Color is the color
        of the glow. This is additive; objects that have their own coloration will keep it but be shaded by our glow.

        Origin is the origin of the effect as a 2-tuple of ints. Hard is a boolean value; setting it to True produces a
        more 'aggressive' glow, simulating a more deeply colored light. If flicker is not positive, there is a 1 in flicker
        chance each draw that the glow effect will not be drawn. If flicker is negative there is a 1 in flicker chance
        that the effect WILL be drawn. It'll draw normally every turn if flicker is 0 or None."""

        if flicker:
            if flicker>0:
                if not random.randint(0,flicker):
                    return
            else:
                if random.randint(0,flicker):
                    return

        if not isinstance(color,tuple):
            try:
                color=pc._color_name_conversion(color)
            except ValueError:
                raise ValueError(
                        "color must be a 3-tuple of integers or a string color name that PyConsoleGraphics accepts, or a 3-tuple of ints, not {0}.".format(
                            color))

        if origin is None:
            origin=self.int_pos()
        xofst,yofst=origin
        #The color's saturation is scaled by the "brightness factor", which is an inverted percentage of the maximum
        #distance from the glow source. The furthest positions from the source are power*2 units away 'cos the effect is
        #square.
        maxdist=power*2

        for x in range(-power,power):
            for y in range(-power,power):
                if x==0 and y==0:
                    continue
                cellx,celly=x+xofst,y+yofst
                try:
                    if not window.terminal[cellx,celly].character:
                        continue
                except IndexError:
                    continue
                brightnessfactor=0.25*(1-math.log(abs(x)+abs(y),maxdist))
                if hard:
                    brightnessfactor*=2
                coloradj=tuple((round(x*brightnessfactor) for x in color))
                existingfg=window.terminal[cellx,celly].fgcolor
                existingbg=window.terminal[cellx,celly].bgcolor
                if existingfg is None:
                    existingfg=(0,0,0,0)
                if hard:
                    #"hard" glow just dims the existing color before applying our own
                    existingfg=tuple((round(x*(1-brightnessfactor)) for x in existingfg))
                newfg=tuple([min(a+b,255) for a,b in zip(existingfg,coloradj)])
                window.terminal[cellx,celly].fgcolor=newfg

    def p_draw(self,surface: pygame.Surface,mainwin: pygcurse.PygcurseWindow):
        """Called each frame after Pygcurse has blitted the character graphics to the screen. This is for things that need to do direct Pygame graphics."""
        pass

    def int_pos(self):
        """Return this obj's pos with x and y cast to ints. Some objects can have float positions."""
        return tuple([int(t) for t in self.pos])

    def __repr__(self):
        return "<{0}.{1} object at {2}>".format(type(self).__module__,type(self).__name__,self.int_pos())

class Player(GameObj):
    """The player object, controlled by the player. This object handles all input from the player, including bringing
    up the injection console.

    .binds is a dict that maps 2-tuples of (keycode, modifiercode) to either callables or action codes; action codes are
    handled specially by other parts of the input handler, callables get called with no parameters whenever that button
    is pressed."""

    #This is a list of all the strings that are acceptable to have as values in .binds; this list isn't automatically
    #kept up to date with the code in .update() so make sure you do that yourself
    action_codes=["Move Up", "Move Down", "Move Left", "Move Right", "Injection Console", "Save & Quit", "Reset Level",
                  "Open Readme"]
    default_binds = {
        main.KeyBinding("up", False, False, False, False)       : "Move Up",
        main.KeyBinding("down", False, False, False, False)     : "Move Down",
        main.KeyBinding("left", False, False, False, False)     : "Move Left",
        main.KeyBinding("right", False, False, False, False)    : "Move Right",
        main.KeyBinding("f1", False, False, False, False)       : "Open Readme",
        main.KeyBinding(";", False, False, False, False)        : "Injection Console",
        main.KeyBinding("escape", False, False, False, False)   : "Save & Quit",
        main.KeyBinding("r", False, False, False, False)        : "Reset Level"
    }
    for k, v in default_binds.items():
        k.action = v

    def __init__(self,startpos):
        super().__init__('@',startpos)
        #The level gives us our console when it starts up.
        self.injectionconsole=None
        self.proxy=PlayerProxy()

        self.binds=None #This gets initalized in our first .update()
        #binds is a dict mapping int scancodes (pygame os-specific scancodes, specifically - the K_Whatever values are
        #automatically altered within Pygame to mean the same thing accross different OSes, so scancode ints do not
        #work cross-platform - but the default keybindings DO work coss-platform because we base them on Pygame's database
        #which has already been translated to the target OS's codes within Pygame) either to callables or strings. The
        #only valid strings are those in action_codes; anything else will cause ValueError upon that key event being
        #processed. If the value is a callable it will be called with no parameters.

        #If this is True, the player is locked out of the injection console; pressing the "bring up console" button
        #just puts a "CONSOLE LOCKED" message up on the screen.
        self.consolelocked=False

    def move(self, movetup, level):
        """Move the player. Returns None if successful, or a GameObj if we hit that GameObj."""
        x,y=self.pos
        move_x, move_y = (max(-1,min(1,n)) for n in movetup)
        newpos=(x+move_x,y+move_y)

        collision=movement_check(newpos,level,self)

        if collision:
            return collision
        else:
            self.oldpos=self.pos
            self.pos=newpos

    def update(self,gamestate):

        #If we need to, initialize our keybind system
        if self.binds is None:
            #Check if GameState has anything to say on the matter - if the user altered his bindings in the settings
            #menu GameState will return a bindings list
            self.binds=gamestate.get_custom_bindings()

            #But if there are no custom binds defined, it'll return None...
            if self.binds is None:
                #so implement the defaults.
                self.binds=self.default_binds.copy()

        #First figure out where we should move,
        #by grabbing Pygame keydown events.
        move_seq = []
        for kbd in gamestate.mainwin.terminal.backend.get_keypresses():
            #First thing we do is convert the keydown event into one of these snazzy KeyBinding objects that handle
            #all the tricky parts of matching keypresses
            pressed=main.KeyBinding(*kbd)

            try:
                binding = self.binds[pressed]
            except KeyError:
                #If we have no binding for it, just ignore it
                continue

            if callable(binding):
                #if it's a callable, we just call it and continue -
                #this is to support binding of IJ console funcs to keyboard keys, which the player can do late-game
                binding()
                continue
            elif binding not in self.action_codes:
                #If it's not a valid action code we raise ValueError
                raise ValueError("'{0}' has invalid action code.".format(str(binding)))

            if binding == "Move Up":
                move_seq.append((0,-1))
            elif binding == "Move Down":
                move_seq.append((0,1))
            elif binding == "Move Left":
                move_seq.append((-1,0))
            elif binding == "Move Right":
                move_seq.append((1,0))
            elif binding == "Open Readme":
                webbrowser.open("README.txt")
            elif binding == "Injection Console":
                if self.consolelocked:
                    gamestate.current_level.consolelockedtimer=60
                    continue
                gamestate.mainwin.blank()
                self.injectionconsole.interact()
                #Also lets add a little aberration as a transition just 'cos
                gamestate.add_fx(fx.chromatic_aberration,2,intensity=3)
            elif binding == "Save & Quit":
                #Yes, the player object handles making the game quit. No, it probably shouldn't.
                gamestate.save_and_quit()
            elif binding == "Reset Level":
                #R brings up the level reset dialogue
                gamestate.reset_level()
                return
        #Check that the controls aren't locked (this is unused, actually)
        if gamestate.controls_locked:
            return
        #We also don't want to do all this stuff if we haven't actually been asked to move
        #('interaction' only happens when the plyobj first walks onto the square)
        interactee = None
        for move in move_seq:
            interactee = self.move(move, gamestate.current_level)
        #Quickly make sure our proxy is updated on our movements so missiles etc. can track us without revealing plyobj to the player
        self.proxy.pos=self.pos
        #Now that we've moved, figure out who, if anyone, we've just walked into (it'll be the return from self.move())
        #If it's None, we moved into empty space. Otherwise, we tell them we just interacted with them.
        if interactee:
            interactee.interact(self,gamestate)

    def get_keycode(self):
        """Used by the player to work with .binds. When called, waits for a keypress, then returns the tuple that
        represents that key/modifier combo in .binds. Use it like me.binds[me.get_keycode()]=my_func, where my_func
        is a function that takes no arguments, which will be called whenever the key is pressed."""
        print("Press a key:")
        while True:
            event=pygame.event.wait()
            if event.type==pygame.locals.KEYUP:
                mod=event.mod
                #Make right or left shift or ctrl keys be both
                return main.KeyBinding(event.key, mod)

    def draw(self,window):
        super().draw(window,(255,255,255))
        self.glow((127,127,127),5,window)

    def kill(self, src, gamestate):
        """'Kill' the player character (teleport him to his starting position)."""
        #Death is cheap; all you really lose is physical progress through the level.
        #And we put down a little skull just to drive the point home as to what happened
        gamestate.current_level.safe_objlist.append(Skull(self.pos))
        self.pos=gamestate.current_level.plystartpos
        gamestate.add_fx(fx.random_chromatic_aberration,4,intensity=8)

class PlayerProxy(GameObj):
    """An object used by things that need to track the player but are also exposed to the injection conosle - bad things
    will happen if the player is allowed access to the player object, so we use this to track it without actually being
    linked to it. It's also handed back by a drone's .bumped for similar reasons."""

    def __init__(self):
        self.pos=(0,0)

class Skull(GameObj):
    """A graphical marker to show that the player character has 'died'. Has no gameplay effect and disappears after 30 frames."""
    def __init__(self,startpos):
        super().__init__('☠',startpos)
        self.countdown=30

    def update(self,gamestate):
        self.countdown-=1
        if not self.countdown:
            gamestate.current_level.remove_obj(self)

class Wall(GameObj):

    def __init__(self,startpos):
        super().__init__('#',startpos)
        self.adjacencies=None
        self.color=(100,100,100)
        self.scrambletimer=0

    def update(self,gamestate):
        if self.scrambletimer:
            self.scrambletimer-=1

        if self.adjacencies is None:
            self.adjacencies=[]
            x, y = self.pos
            for dx in (-1,0,1):
                for dy in (-1,0,1):
                    theobj=gamestate.current_level.get_at((x+dx,y+dy))
                    if isinstance(theobj,Wall):
                        self.adjacencies.append(theobj)
                        #self.adjacencies.append((x+dx,y+dy))

    def draw(self,window):
        #all this stuff only matters if there are actually walls next to us
        if self.adjacencies:
            #we have a 1/1000 chance of randomly "mutating" our color
            if random.randint(1,1000)==1:
                r, g, b = self.color
                r+=random.randint(-64,96)
                g+=random.randint(-64,96)
                b+=random.randint(-64,96)
                r=clamp(r,255,80)
                g=clamp(g,255,80)
                b=clamp(b,255,80)
                self.color=r,g,b
            else:
                #otherwise our color is the average of the colors of our adjacent walls
                rs=[]
                gs=[]
                bs=[]
                xf=math.floor((random.triangular(0,1,0)*32)+16) #exaggeration factor - the higher this is the more the square mean function amplifies outliers
                #xf=162
                for thewall in self.adjacencies:
                    r, g, b = thewall.color
                    rs.append(r**xf)
                    gs.append(g**xf)
                    bs.append(b**xf)
                r=math.floor(math.pow(sum(rs)/(len(rs)),1/xf)) #Raising a number to the power of a number inverted is the same as taking that root of it
                g=math.floor(math.pow(sum(gs)/(len(gs)),1/xf))
                b=math.floor(math.pow(sum(bs)/(len(bs)),1/xf))
                r=clamp(r,192,80)
                g=clamp(g,192,80)
                b=clamp(b,192,80)
                self.color=(r,g,b)

        super().draw(window,color=self.color)

    def scramble_color(self,hard=False):
        """Set this wall's color to a random color. The hard boolean flag determines if the color will be totally random
        (if false) or forced to extreme values (if true)."""
        #We don't want to "scramblespam" when, for example, something continually bumps into us
        if self.scrambletimer:
            self.scrambletimer=3
            return

        self.scrambletimer=3

        if hard:
            newcolor=(random.randint(0,2)*127,random.randint(0,2)*127,random.randint(0,2)*127)
            for thewall in self.adjacencies:
                thewall.color=newcolor
        else:
            newcolor=(random.randint(0,255),random.randint(0,255),random.randint(0,255))
        self.color=newcolor

    def interact(self,player,gamestate):
        #Being bumped into by the player makes a much bigger color splash
        self.scramble_color(hard=True)
        if not main._distributable:
            print("Bumped into wall at",self.pos)

    def collide(self,other,nonphysical=False):
        #We scramble color when something hits us
        if not nonphysical:
            self.scramble_color()
        return True

    def __getstate__(self):
        #We just need to destroy our links to the other walls so we don't piss Pickle off
        self.adjacencies=None
        return self.__dict__

class Trigger(GameObj):

    def __init__(self,graphic,pos,target):
        super().__init__(graphic,pos)
        self.target=target

    def _trip(self):
        """Usually called by a Trigger's interact(). Causes the trigger's target to do whatever it does."""
        self.target.trigger()

class Gate(GameObj):
    """An object that, when triggered, toggles whether or not the player can walk through it."""

    def __init__(self,startpos,open=False):
        super().__init__('█',startpos)
        self.open=open

    def update(self,gamestate):
        if self.open:
            self.graphic=' '
        else:
            self.graphic='█'
            #We also make sure we can "close on" the player; if he's in the doorway when we close, he'll get bounced back
            if gamestate.current_level.plyobj.pos == self.pos:
                gamestate.current_level.plyobj.pos=gamestate.current_level.plyobj.oldpos

    def trigger(self):
        self.open=not self.open

    def collide(self,other,nonphysical=False):
        return not self.open

class Levelporter(GameObj):
    """The objective in each level is to walk in to this object. When the player stands on it, the game proceeds to
    the next level."""

    def __init__(self,startpos,targetlevel):
        super().__init__('⌂',startpos)
        self._targetlevel=targetlevel
        self.fxintensity=0

    def update(self,gamestate):
        ply=gamestate.current_level.plyobj
        plypos=ply.pos
        plydist=round(distance(self.pos,plypos))

        #Don't run the trace if he's standing on us, it makes errors
        if plydist and plydist < 3:
            raycast_hit=raycast_to(self.pos,plypos,gamestate.current_level,ignore_objs=[self])
        else:
            raycast_hit=ply

        #If he can actually reach us, and he's within three tiles, play an effect
        if raycast_hit is ply and plydist < 3:
            self.fxintensity=(3-plydist)
        else:
            self.fxintensity=0


    def interact(self,plyobj,gamestate):
        gamestate.mark_current_level_revisitable()
        gamestate.change_level(self._targetlevel)

    def draw(self,window):
        self.glow((187,167,167),8,window)
        super().draw(window,(187,167,167))

    def p_draw(self,surface,mainwin):
        if self.fxintensity:
            fx.random_chromatic_aberration(surface,self.fxintensity+1)

class PressurePlate(Trigger):
    """A trigger that fires whenever something enters or leaves its tile. Initialize with singleimpulse=True to trigger only when something enters."""

    def __init__(self,startpos,target,singleimpulse=False):
        super().__init__('♁',startpos,target)
        self.down=False
        self.singleimpulse=singleimpulse

    def update(self,gamestate):
        if self.down:
            self.down=False
            wasdown=True
        else:
            wasdown=False

        for theobj in gamestate.current_level.objlist + gamestate.current_level.safe_objlist + [gamestate.current_level.plyobj]:
            if theobj.int_pos() == self.pos and not isinstance(theobj,(no_collide_objs,no_floor_contact_objs)):
                self.down=True

        #Normally, something getting on the plate trips it once, and leaving trips it again.
        #If we're initialized with singleimpulse=True, only entering the plate trips it; leaving does not.
        if self.singleimpulse:
            #If we're down and we weren't down before, trip.
            if self.down and not wasdown:
                self._trip()
        else:
            #If we were down but aren't, or are down but weren't, trip. If things stay the same, don't.
            if not (self.down == wasdown):
                self._trip()

    def draw(self,window):
        #We don't want to draw ourself if we're down; player should see the thing on top of us instead.
        if not self.down:
            #single-impulse plates are greenish
            if self.singleimpulse:
                color="palegreen"
            else:
                color=None

            super().draw(window,color)

class Boulder(GameObj):
    """An object with momentum. Any time it is moved, its momentum is set to the direction it moved, and it keeps moving
    according to that momentum. I.e. setting its momentum to (0,-1) will make it move up, and if it is at (10, 10) and
    and you move it to (10, 11), that will set its momentum to (0,1) and make it move down every turn."""

    def __init__(self,startpos, momentum=(0,1)):
        super().__init__('○',startpos)
        x, y = startpos
        self.momentum = momentum
        self.was_moved_this_tick = False

    def __setattr__(self, key, value):
        if key == "pos" and hasattr(self, "pos"):
            x1, y1 = self.pos
            x2, y2 = value
            dx, dy = x2 - x1, y2 - y1
            if dx: dx = int(math.copysign(1, dx))
            if dy: dy = int(math.copysign(1, dy))
            self.momentum = (dx, dy)
            self.was_moved_this_tick = True
        super().__setattr__(key, value)

    def update(self, gamestate):
        if self.was_moved_this_tick:
            self.was_moved_this_tick = False
            return

        x, y = self.pos
        dx, dy = self.momentum

        x += dx #Note that we don't actually set our position until after the collision check
        y += dy

        #Movement_check returns None if we're not blocked.
        if not movement_check((x,y),gamestate.current_level,self):
            self.pos = (x, y)
        else:
            self.momentum = (0,0)

        self.was_moved_this_tick = False

class GravPad(GameObj):
    """An object that finds objects range tiles above it (the -y direction), and pushes them one space in the direction
    specified in gravity_direction every turn."""

    gravity_direction_symbol_map={(0,0) : "∙", (0, -1) : "↑", (0, 1) : "↓", (1,0) : "→", (-1,0) : "←",
                        (1,1) : "↘", (-1,1) : "↙", (-1,-1) : "↖", (1,-1) : "↗"}

    def __init__(self, startpos, gravity_direction, range):
        super().__init__("♨", startpos)
        self.gravity_direction=gravity_direction
        self.range=range
        self.gravity_direction_is_invalid=False

    def update(self,gamestate):
        #If gravity_direction is invalid (not a tuple of two ints), we can't function.
        if self.gravity_direction not in self.gravity_direction_symbol_map:
            self.gravity_direction_is_invalid=True
            return
        else:
            self.gravity_direction_is_invalid=False

        already_pushed = set()

        ox, oy = self.pos
        step = -int(math.copysign(1, self.range))
        affected_locations = [(ox, oy + dy) for dy in range(step, (-self.range) + step, step)]
        pushx, pushy = self.gravity_direction

        for thelocation in affected_locations:
            theobj = gamestate.current_level.get_at(thelocation, include_player = True, exclude = no_collide_objs + tuple(already_pushed))
            if not theobj: continue

            if isinstance(theobj, Player):
                theobj.move(self.gravity_direction, gamestate.current_level)
            elif isinstance(theobj, (no_collide_objs)+(Wall,Gate)):
                continue
            else:
                subjx, subjy = theobj.pos
                subjx, subjy = subjx + pushx, subjy + pushy
                if not movement_check((subjx, subjy), gamestate.current_level, theobj):
                    theobj.pos = (subjx, subjy)
            already_pushed.add(theobj)

    def draw(self, window):
        super().draw(window)
        x, y = self.pos

        try:
            symbol=self.gravity_direction_symbol_map[self.gravity_direction]
        except (KeyError, TypeError):
            symbol="?"
            self.gravity_direction_is_invalid=True


        #For every tile we affect that is empty (nothing's been drawn yet), draw the symbol
        #we use to indicate our range.
        ox, oy = self.pos
        step = -int(math.copysign(1, self.range))
        affected_locations = [(ox, oy + dy) for dy in range(step, (-self.range)+step, step)]

        time_offset = 0
        for x, y in affected_locations:
            if self.gravity_direction_is_invalid:
                color = "red"
            else:
                t = math.sin((time.monotonic() - time_offset) * 2)
                color = (64, 64, 192 + math.floor(t * 64))
                time_offset += 0.75 / self.range

            if window.terminal[x, y].character==" ":
                window.terminal[x,y].character=symbol
                window.terminal[x, y].fgcolor=color
            self.glow(color,2,window,origin=(x,y),hard=True)

class SignalSplitter():
    """A trigger that, when tripped by another trigger, triggers more than one target. This is a virtual object; it isn't
    actually a GameObj or Trigger."""

    def __init__(self, targets):
        self.targets=targets

    def trigger(self):
        for thetarget in self.targets:
            thetarget.trigger()

class MissileTrail(GameObj):
    """A purely graphical effect that Missile objects leave trailing behind them. Removes itself after around 5 turns, that is, 1/3 of a second."""

    timer_to_graphics={5:('█',(255,0,0)),
                       4:('▓',(200,200,0)),
                       3:('▓',(127,127,0)),
                       2:('▒',(107,107,127)),
                       1:('░',(127,127,127)),
                       0:('░',(127,127,127))}

    def __init__(self,startpos,timer=6):
        super().__init__("█",startpos)
        self.timer=timer

    def update(self,gamestate):
        if self.timer:
            self.timer-=1
        else:
            #We have a 1 in 3 chance of being removed every turn once our timer hits 0;
            #this is so that the smoke trail "breaks up" at the end
            if not random.randint(0,2):
                gamestate.current_level.remove_obj(self)

        #1 in 5 chance every turn that we create a smoke puff next to us if we're at yellow-stage.
        #1 in 12 if we're less
        #Slightly less, actually, since we check if there's something there first and give up if there is.
        if self.timer > 3:
            chance=4
        else:
            chance=11
        if not random.randint(0,chance):
            x, y = self.pos
            puffpos=(x+random.randint(-1,1),y+random.randint(-1,1))
            if not movement_check(puffpos,gamestate.current_level,None):
                puff=MissileTrail(puffpos,2)
                gamestate.current_level.safe_objlist.append(puff)

    def draw(self,window):
        try:
            graphic, color=self.timer_to_graphics[self.timer]
        except KeyError:
            graphic, color=self.timer_to_graphics[5]
        self.graphic=graphic
        super().draw(window,color)
        if self.timer >= 3:
            self.glow(color,3,window,hard=True)


class Missile(GameObj):
    """An object that moves towards its target until it encounters another object (except pressureplates and such).
    Whatever object it collides with is removed from the game, unless it's a wall or the player. Walls are unaffected;
    hitting the player moves him back to where he started the level.

    If the missile's target is somehow invalid, the missile destroys itself harmlessly."""

    dir_char_map={(0,1) : "|", (0,-1) : "|", (1,0): "―", (-1,0) : "―", (1,1): "\\", (-1,-1) : "\\", (-1,1) : "/", (1,-1): "/", (0,0) : ""}

    def __init__(self,startpos,target):
        super().__init__("|",startpos)
        self.target=target

    def update(self,gamestate):
        #tx and ty stand for Target x & y
        #The player can set our target to None, which obviously doesn't have pos and raises AttributeError
        #If this happens, we just destroy ourself (the idea being that the missile, having no target, has crashed
        #into the ground and detonated harmlessly).
        try:
            tx, ty = self.target.int_pos()
        except AttributeError:
            #If our target is somehow invalid, we self-destruct
            self.kill(self,gamestate)
            return

        x, y = self.pos

        #Get a direction vector leading to our target (directionx, directiony)
        dx = tx - x
        dy = ty - y

        #And clamp it to -1,0,1
        if abs(dx) > 1:
            dx=dx//abs(dx)
        if abs(dy) > 1:
            dy=dy//abs(dy)

        #If we're on top of the target, but we haven't collided with it (because it's non-collidable, or because it's
        #not actually in the level,) self-destruct
        if dx == dy == 0: self.kill(self, gamestate)

        #Do our cool face-the-direction-you're-moving effect
        self.graphic=self.dir_char_map[(dx, dy)]

        newpos=(x+dx,y+dy)

        #If we're about to hit something...
        hit=movement_check(newpos,gamestate.current_level,self)
        if hit:
            if isinstance(hit, Wall):
                #We don't do anything to walls.
                pass
            elif isinstance(hit, Player):
                #if we hit the player, call its "kill" function
                hit.kill(self, gamestate)
                #We also add a special longer, stronger screenshake when we kill the player
                gamestate.add_fx(fx.screenshake,6,intensity=6)
            else:
                #For everything else, there's Mas- err, I mean, destroy it.
                #gamestate.current_level.remove_obj(hit)
                hit.kill(self,gamestate)

            #In any case, that's the end of this missile.
            self.kill(self,gamestate)
            return


        #And if we haven't hit anything, we can actually move.
        #But spawn a flame effect behind us first!
        gamestate.current_level.safe_objlist.append(MissileTrail(self.pos))
        self.pos=newpos


    def kill(self,src,gamestate):
        """Produce a purely graphical explosion effect and remove this missile from the level."""
        level=gamestate.current_level
        #spawn a cool explosion effect
        level.safe_objlist.append(MissileTrail(self.pos,8))
        x,y=self.int_pos()
        for xofst in range(-1,2):
            for yofst in range(-1,2):
                newpos=(x+xofst,y+yofst)
                if not movement_check(newpos,level,None) and random.randint(0,1):
                    level.safe_objlist.append(MissileTrail(newpos,6))

        #and screenshake!
        gamestate.add_fx(fx.screenshake,3)

        super().kill(self,gamestate)

class MissileLauncher(GameObj):
    """An object that spawns missiles directed at its target at rate frames per missile
    (default of 30; game runs at 15 fps). If addsafe is True, they will be added to the
    safe_objlist instead of the main objlist and be inaccessible from the injection console."""

    def __init__(self,startpos,target,rate=30,addsafe=False):
        super().__init__("",startpos)
        self.target=target
        self.refireclock=self.rate=rate
        self.addsafe=addsafe

    def __setattr__(self, key, value):
        if key == "target":
            if value is not None and not hasattr(value, "int_pos"):
                raise ValueError("Not a valid target")
        super().__setattr__(key, value)

    def update(self,gamestate):
        if self.target is None: return

        #If it's time to launch a missile...
        self.refireclock-=1
        if self.refireclock <= 0:
            self.refireclock=self.rate
        else:
            return

        #Create it and add it to the appropriate list
        #We make the missile at our own pos; it won't hit us, because it looks ahead
        missile=Missile(self.pos,self.target)

        if self.addsafe:
            gamestate.current_level.safe_objlist.append(missile)
        else:
            gamestate.current_level.objlist.append(missile)

    def draw(self,window):
        try:
            refirefrac=self.refireclock/self.rate
            if refirefrac < 0: raise ValueError
            color=(127+round(64*(1-refirefrac)),127+round(64*refirefrac),127) #smooth slide from green to red
            super().draw(window,color)
        except (ZeroDivisionError, ValueError):
            super().draw(window)

    def kill(self,src,gamestate):
        #Launchers also produce a big-ass explosion when they die
        level=gamestate.current_level
        level.safe_objlist.append(MissileTrail(self.pos,8))
        x,y=self.int_pos()
        for xofst in range(-2,4):
            for yofst in range(-2,4):
                newpos=(x+xofst,y+yofst)
                if not movement_check(newpos,level,None) and random.randint(0,1):
                    level.safe_objlist.append(MissileTrail(newpos,7))

        #and screenshake!
        gamestate.add_fx(fx.screenshake,6,intensity=6)

        super().kill(src,gamestate)

class SafePressurePlate(GameObj):
    """A relative of the pressureplate that only works on gates. Makes sure
    the gate can't be \"forced\" open; the only way to make it stay open
    is to depress the pressure plate. Also disallows modifying the target's
    .collide method."""

    def __init__(self,startpos,target):
        super().__init__("♁",startpos)
        self.target=target

    def update(self, gamestate):
        #We do nothing if our target isn't a gate
        if not isinstance(self.target, Gate):
            return

        self.target.open=False
        for theobj in gamestate.current_level.objlist+gamestate.current_level.safe_objlist+[gamestate.current_level.plyobj]:
            if theobj.int_pos()==self.pos and not isinstance(theobj,(no_collide_objs,no_floor_contact_objs)):
                self.target.open=True

        #Make sure the player doesn't mess with its .collide either...
        if self.target.collide is not GameObj.collide:
            self.target.collde=GameObj.collide

    def draw(self,window):
        try:
            if not self.target.open:
                #We don't want to draw if we're not open...
                super().draw(window,"indianred")
        except AttributeError:
            #...but our target might be invalid. if it is, draw as a deeper red.
            super().draw(window,"red")


class AttributeEnforcer(GameObj):
    """A gameobj that constantly re-sets certain object's attributes to what they are at the beginning of the level.
    The inputdict parameter maps GameObj-s to tuples of strings; these are the attributes of the object that you want
    to keep at their initial values. It gets converted internally to .attribdict, which maps attribute names to
    tuples of (GameObj, forcedvalue)."""

    def __init__(self,startpos,inputdict):
        super().__init__("☑",startpos)
        self.attribdict={}
        #When an attribute alteration trips the enforcer, it gets added to this set. The line-and-circle effect is
        #then drawn in red for that object.
        self.invalidatedobjs=set()
        for thekey, thevalue in inputdict.items():
            if isinstance(thevalue,str):
                raise TypeError("The value of the mapping {0}:{1} in inputdict is a string; you probably forgot to 1-tuple-ize it. ({1}) is not a tuple, ({1},) is.")
        self.inputdict=inputdict
        #This is a timer/counter that is set to 40 (game runs at 15fps) whenever the object sees one of its watched
        #parameters has been changed and is decremented every frame. If it is non-zero, we're drawn as a red,
        #crossed-out checkbox, to help the player work out that this object is responsible for re-setting his parameters.
        self.graphicreset=0
        #The lines that connect the enforcer to its enforce-ees fades out slowly, being at full strength when the level
        #starts and whenever the enforcer is tripped
        self.linealpha=255
        #We draw all our lines and circles to a Surface and then blit that Surface on to the window, so we can do
        #transparency properly
        self.gubbinsurface=None

    def update(self,gamestate):
        #On our first update(), we should grab all the parameters.
        if hasattr(self,"inputdict"):
            for obj, params in self.inputdict.items():
                for theparam in params:
                    self.attribdict[(obj,theparam)]=getattr(obj,theparam)
            del self.inputdict
        else:
            #After that, just keep setting those values

            #Hold the "this is the one that cause the problem" effect up until graphicreset expires
            if not self.graphicreset:
                self.invalidatedobjs.clear()

            keystodelete=[]
            for key in self.attribdict:
                obj, param = key
                if obj not in gamestate.current_level.objlist+gamestate.current_level.safe_objlist:
                    keystodelete.append(key)
                    continue
                if not self.attribdict[key]==getattr(obj,param):
                    self.graphicreset=40
                    setattr(obj,param,self.attribdict[key])
                    self.invalidatedobjs.add(obj)

            for thekey in keystodelete:
                del self.attribdict[key]

    def draw(self,mainwin):
        x, y=self.pos
        cell = mainwin.terminal[x,y]

        if self.graphicreset:
            self.graphicreset-=1
            cell.character = "☒"
            cell.fgcolor = "red"
            self.glow("red",40,mainwin,hard=True)
            self.linealpha=255
        else:
            cell.character = "☑"
            cell.fgcolor = "green"
            self.glow("green",6,mainwin)

    def p_draw(self,surface,mainwin):
        #@TODO: Refactor this to not be TRASH
        if self.gubbinsurface is None:
            self.gubbinsurface=pygame.Surface(pygame.display.get_surface().get_size())

        if self.linealpha:
            self.linealpha -= 1
            normaleffectcolor = pygame.Color(0, self.linealpha, 0)
            activeeffectcolor=pygame.Color("red")
            cellwidth = mainwin.terminal.backend.cellwidth

            self.gubbinsurface.fill(pygame.color.Color(0, 0, 0, 0))
            for theobj in set([key[0] for key in self.attribdict.keys()]):
                if theobj in self.invalidatedobjs:
                    effectcolor=activeeffectcolor
                else:
                    effectcolor=normaleffectcolor

                pygame.draw.circle(self.gubbinsurface,effectcolor,get_px_pos(theobj,mainwin),cellwidth,1)
                pygame.draw.aaline(self.gubbinsurface,effectcolor,get_px_pos(self,mainwin),get_px_pos(theobj,mainwin),1)

                surface.blit(self.gubbinsurface, (0, 0), None, pygame.BLEND_RGBA_ADD)

    def __getstate__(self):
        #Pygame Surface-s don't keep well, even the ones that are just in memory.
        #Luckily, we only store the thing so we don't have to take the time to make a new one each time, so...
        state = self.__dict__.copy()
        del state["gubbinsurface"]
        return state

    def __setstate__(self, state):
        self.__dict__ = state.copy()
        self.gubbinsurface = None

class Teleporter(GameObj):
    """Anything that enters the same square as this object is teleported to its dest coordinates."""

    def __init__(self,startpos,dest):
        super().__init__("˽",startpos)
        self.dest=dest
        self.tpeffecttimer=0

    def update(self,gamestate):
        occupant=gamestate.current_level.get_at(self.pos,exclude=(self,),include_player=True)
        if occupant and occupant is not self:
            if isinstance(occupant, Boulder):
                momentum = occupant.momentum
                occupant.pos = self.dest
                occupant.momentum = momentum
            else:
                occupant.pos=self.dest
            self.tpeffecttimer=5


    def draw(self,window):
        if self.tpeffecttimer:
            super().draw(window,"steelblue")
            self.glow("blue",4,window)
            self.glow("blue",12,window,origin=self.dest)
            self.tpeffecttimer-=1
            #The effect crashes if it's too close to the edge of the screen
            if 1 < self.pos[0] < 80 and 1 < self.pos[1] < 40:
                posx, posy = self.pos
                posx += 0.5; posy += 0.5
                self.pixel_pos=window.terminal.backend.cell_to_px((posx,posy))
            else:
                self.pixel_pos=None

            if 1 < self.dest[0] < 80 and 1 < self.dest[1] < 40:
                destx, desty = self.dest
                destx += 0.5; desty += 0.5
                self.dest_pixel_pos=window.terminal.backend.cell_to_px((destx, desty))
            else:
                self.dest_pixel_pos=None
        else:
            super().draw(window)

    def p_draw(self,surface,mainwin):
        if self.tpeffecttimer:
            power = self.tpeffecttimer * 5
            if self.pixel_pos:
                fx.chroma_warp(surface,*self.pixel_pos,radius=power,power=power)
            if self.dest_pixel_pos:
                fx.chroma_warp(surface,*self.dest_pixel_pos,radius=power + 10,power=power)


class WallProxy:
    """It's impractical to have an AttributeEnforcer enforcing the positions of all the walls in the level, so instead
    the drone will set its .bumped to a WallProxy instead of any Wall. A WallProxy is basically just a null object with
    a .pos attribute; walls aren't very interesting."""

    def __init__(self,wall):
        if not isinstance(wall,Wall):
            raise TypeError("Tried to construct a WallProxy around a {0}: not a gameobjs.Wall!".format(wall.__class__))
        self.pos=wall.pos

class DroneProxy:
    """The object a Drone passes into its think() function instead of itself. This allows the player-written code to, say,
    read the drone's position, but not set it."""


    def __init__(self,drone):
        if not isinstance(drone,Drone):
            raise TypeError("Tried to construct a DroneProxy around a {0}: not a gameobjs.Drone!".format(drone.__class__))
        self.__drone=drone

    #The property decorator here just makes the attribute read-only
    @property
    def pos(self):
        """The drone is at this position on the screen. Read-only."""
        return self.__drone.pos

    @property
    def bumped(self):
        """If the drone tried to move into occupied space last turn, this references the object that was occupying that space."""
        return self.__drone.bumped

    @property
    def color(self):
        """The drone's icon will be drawn in this color. Read-write."""
        return self.__drone.color

    @color.setter
    def color(self,value):
        self.__drone.color=value

    def move(self,vec):
        self.__drone.move(vec)


class NewDroneProxy():
    """Old-style DroneProxy objects used properties to implement 'read-only' versions of the attributes we wish to expose to the
    drone API. These necessarily had to contain a reference to their drone object, pretty much defeating the purpose of allowing
    the drone code access to information about the drone without access to the drone itself. New-style proxy objects instead only
    hold data, which is updated by the owning drone's update() function. These contain no reference to the Drone object itself."""

    def __init__(self,drone):
        self.color=drone.color
        self.pos=drone.pos
        self.bumped=drone.bumped
        self._nextmove=None

    def update_out(self,drone):
        """Pull out all the data we need to replicate from the given drone, storing no actual reference to the drone itself."""

        self.pos=drone.pos
        self.bumped=drone.bumped

    def update_in(self,drone):
        """Push any changes made to the proxy object during a think() call back to the drone."""
        drone.color=self.color
        if self._nextmove:
            drone.move(self._nextmove)
            self._nextmove=None

    def int_pos(self):
        return (int(self.pos[0]),int(self.pos[1]))

    def move(self,x,y):
        self._nextmove=(x,y)


class Drone(GameObj):
    """
    An object that can move around and trip pressureplates and such, controlled by a user-defined think() function.
    Be careful; it can only move once per tick (think is executed once each tick)!

    The think function should accept only one parameter; self. Think() does not have access to all the attributes and methods
    of its drone. The API it does get is as such:

    methods:
    self.move(x,y) : Move the drone in the given direction (i.e. move(0,1) moves it down the screen, move(1,0) moves it right)

    attributes:
    self.pos : A 2-tuple containing the drone's position.
    self.color : A 3-tuple of RGB values; the drone icon will be this color.
    self.bumped : If the drone bumped into something last tick (tried to move into the space it occupied), this references that object.

    think() can also put new attributes on self if it wants to have persistent state.

    Anything that normally sends text to stdout will print to the injection console.
    """

    def __init__(self,startpos,think=null_func):
        super().__init__("♙",startpos)
        self.move_vec=(0,0)
        self.think=think
        self.color=(255,255,255)
        self.bumped=None
        self.proxy=NewDroneProxy(self)

    def update(self,gamestate):
        #Note that since think is always monkey-patched, always defined outside the Drone class,
        #it does not get the self parameter.

        if self.move_vec[0] or self.move_vec[1]:
            dx, dy = self.move_vec
            self.move_vec=(0,0)
            newpos=(self.pos[0]+dx,self.pos[1]+dy)

            self.bumped=movement_check(newpos,gamestate.current_level,self)
            if self.bumped:
                #Can't let drones move the walls around, that'd be bad
                if isinstance(self.bumped,Wall):
                    self.bumped=WallProxy(self.bumped)
                elif isinstance(self.bumped,Player):
                    self.bumped=self.bumped.proxy
                return
            else:
                self.pos=newpos
                return

        self.proxy.update_out(self)
        main.call_with_timeout(self.think,(1/30),self.proxy) #a drone think can only take up to 1/30th of a second to complete
        self.proxy.update_in(self)

    def draw(self, window):
        super().draw(window, self.color)

    def move(self,vec):
        """Try to move along a given vector tuple. Returns nothing. Note that the movement only takes effect the frame after move() is
        called, and only the last move() command actually does anything."""

        #d is for delta
        dx, dy = vec

        #clamping again
        if abs(dx) > 1:
            dx=dx//abs(dx)
        if abs(dy) > 1:
            dy=dy//abs(dy)

        #We can't actually do the things needed to move ourself without map data, so we wait 'till next update.
        self.move_vec=(dx, dy)

    def upload(self,func):
        """
        Set this drone's think() to the given function. This is added to the
        injection console namespace in lieu of the drone itself. The following
        is a description of the drone API, which is also described in the readme:

        drone.think() is a method called once for each game tick (i.e. the code
        you upload with upload() gets run every time anything happens)
        and is the means by which the drone decides what to do.

        drone.think() is passed one parameter, self, which is a proxy to the
        drone object. It has the following attributes and methods:

        self.move(x,y) : Move the drone in the given direction / along the given
        2d vector (i.e. move(0,1) moves it down the screen, (1,0) moves it right)

        self.pos : A 2-tuple containing the drone's position.
        self.color : A 3-tuple of RGB values; the drone icon will be this color.
        self.bumped : If the drone bumped into something last tick
        (tried to move into the space it occupied), this references that object.

        So for example, if you want a drone that just moves left, you'd say:
        def mythink(self):
            self.move(1,0)

        Indentation is important:

        def mythink(self):
        self.move(1,0)

        won't work.

        Type a blank line to stop defining a function.
        The ... instead of the > means you're still inside the
        function definition.

        Then call upload(mythink) in the console, and the drone will do what you
        told it to in the function. Note, however, that the think() function
        starts anew each tick. You can't do something like "while true self.move(1,0)"
        because the drone gets one chance to do things for each time think() is called,
        and that function will just think about moving left forever without
        actually doing so.

        Don't forget that you can store your own attributes on the proxy object;
        that's the only way to have state that persists across turns.
        """
        self.think=func

class CombatAndroid(GameObj):
    """An object that hunts down enemy objects and shoots them with deadly lasers."""

    def __init__(self,startpos,targets=[]):
        super().__init__("©",startpos)
        self.targets=targets
        self.target=None

    def is_enemy(self,obj):
        """Determine whether or not the given object is an enemy unit and should be destroyed. If it is, returns true."""

        if isinstance(obj,Player):
            return True
        elif obj in self.targets:
            return True
        else:
            return False

    def update(self,gamestate):
        #If our target is None, we need a new one.
        if not self.target:
            #Build an iterator (ptgts) containing all active gameobjs that are enemies
            #(self.is_enemy() returns true on them)
            ptgts1=(obj for obj in gamestate.current_level.objlist if self.is_enemy(obj))
            ptgts2=(obj for obj in gamestate.current_level.safe_objlist if self.is_enemy(obj))
            if self.is_enemy(gamestate.current_level.plyobj):
                ptgts=itertools.chain(ptgts1,ptgts2,(gamestate.current_level.plyobj,))
            else:
                ptgts=itertools.chain(ptgts1,ptgts2)

            current_target=None
            current_dist=float("+inf")
            #find closest object in ptgts
            for obj in ptgts:
                objdist=distance(obj, self)
                if objdist < current_dist:
                    current_target=obj
                    current_dist=objdist
            self.target=current_target

        #Now we try shooting at our target...
        if raycast_to(self.int_pos(), self.target.int_pos(), gamestate.current_level) == self.target:
            print("Can see target!")
        #print(path(self.int_pos(),self.target.int_pos(), gamestate.current_level, gamestate.mainwin))

class LaserTurret(GameObj):
    """An object that fires deadly lasers at the nearest enemy target."""

    def __init__(self,startpos):
        super().__init__('⌀',startpos)
        self.target=None
        self.cooldown=0
        self.fire_laser_next_draw = None
        self.hit = None

    def is_enemy(self,obj):
        """Given a GameObj obj, returns True if the object is an enemy and thus should be destroyed."""

        if isinstance(obj,Player):
            return True

        if isinstance(obj, Missile) and distance(self, obj) < 10:
            #Is it actually going to hit us though?
            if obj.target is self: return True

            missile, target, me = pc.Pos(obj.pos), pc.Pos(obj.target.int_pos()), pc.Pos(self.pos)
            missile_going = (target - missile).normalized()
            towards_me = (me - missile).normalized()
            if missile_going * towards_me >= 0.85:
                return True

    def update(self,gamestate):
        # We only harm the player & missiles; all other objects are unaffected
        if self.hit:
            if isinstance(self.hit, (Player, Missile)):
                self.hit.kill(self, gamestate)
            self.target = None
            self.hit = None

        #If our target is None, we need a new one.
        if not self.target:
            #Build an iterator (ptgts) containing all active gameobjs that are enemies
            #(self.is_enemy() returns true on them)
            ptgts1=(obj for obj in gamestate.current_level.objlist if self.is_enemy(obj))
            ptgts2=(obj for obj in gamestate.current_level.safe_objlist if self.is_enemy(obj))
            if self.is_enemy(gamestate.current_level.plyobj):
                ptgts=itertools.chain(ptgts1,ptgts2,(gamestate.current_level.plyobj,))
            else:
                ptgts=itertools.chain(ptgts1,ptgts2)

            closest_target=None
            closest_dist=float("+inf")
            #find closest object in ptgts
            for obj in ptgts:
                objdist=distance(obj, self)
                if objdist < closest_dist:
                    closest_target=obj
                    closest_dist=objdist
            self.target=closest_target
            if not closest_target:
                return

        if self.cooldown <= 0:
            #We check to see if there's anything in the way before we do the expensive laser calculation
            will_hit = raycast_to(self.pos,self.target.int_pos(), gamestate.current_level, ignore_objs = [self])
            if  will_hit == self.target:
                self.cooldown=0
                #For some reason it took me two years to realize this doesn't work -
                # We can't fire the laser until p_draw, otherwise the screen we read isn't the screen the player
                # will see!!!
                self.fire_laser_next_draw = gamestate
            else:
                #if we don't have LOS to our target we forget about it - otherwise we don't recheck
                #for the closest target until the last one is dead or hidden
                #print(f"No LOS to {self.target}: {will_hit} in the way", file=sys.__stdout__)
                self.target=None
        else:
            self.cooldown-=1

    def draw(self,window):
        if self.target:
            color="red"
        else:
            color="green"
        super().draw(window,color)
        self.glow(color,4,window)

    def p_draw(self,surface,mainwin):
        if self.fire_laser_next_draw and self.target:
            self.hit = laser(self.pos, self.target.int_pos(), self.fire_laser_next_draw, beamcolor="red")
            self.fire_laser_next_draw = None

class LaserBeam(GameObj):
    """A purely graphical object that's responsible for drawing awesome laz0r beams."""

    def __init__(self,src,dest,color,lifetime=3):
        super().__init__("",(-1,-1))
        self.src=src
        self.dest=dest
        self.color=pc._color_name_conversion(color)
        self.lifetime=lifetime

    def update(self,gamestate):
        if self.lifetime:
            self.lifetime-=1
        else:
            gamestate.current_level.remove_obj(self)

    def draw(self,window):
        origin=window.terminal.backend.px_to_cell(tuple(self.dest))
        self.glow(self.color,8,window,origin=origin)

    def p_draw(self,surface,mainwin):
        pygame.draw.line(surface,self.color,self.src,self.dest)


class Lvl1Keypad(Trigger):
    """A security keypad that asks for a secret passphrase, which is stored in its .keyphrase parameter.
    If the correct code is entered, this object opens the door it is connected to."""

    def __init__(self,startpos,target):
        super().__init__('፬',startpos,target)
        #People were having problems realising the keycode is a string, not an int
        #So now it's made of letters, not numbers.
        self.keyphrase=""
        vowels=('a','e','i','o','u','oo')
        consonants=('b','c','d','f','g','h','j','k','l','m','n','p','q','r','s','t','v','w','x','y','z','ch','ck','sh',)
        while True:

            for t in range (20):
                for tt in range(random.randint(2,4)):
                    if random.random() > 0.5: #50% chance of either type of sylable
                        self.keyphrase+=(random.choice(consonants)+random.choice(vowels)+random.choice(consonants)) #Random vowel, random consonant, random vowel
                    else:
                        self.keyphrase+=(random.choice(vowels)+random.choice(consonants)) #Random consonant, random vowel
                self.keyphrase+=" "

            #check for naughty words
            for nastyword in {'\x73\x68\x69\x74', '\x72\x61\x70\x65', '\x66\x75\x63\x6b', '\x61\x73\x73', '\x63\x75\x6e\x74'}:
                if nastyword in self.keyphrase:
                    self.keyphrase=""
                    break
            else:
                #This breaks the while True and only happens if the above break doesn't happen
                break

    def interact(self,player,gamestate):
        player.pos=player.oldpos
        win=gamestate.mainwin.terminal
        oldfg=win.fgcolor
        win.fgcolor=(0x39,0xFF,0x14)
        gamestate.mainwin.blank()
        gamestate.mainwin.put_cursor((0,4))
        gamestate.mainwin.put_line("Please enter your 20-word authorization keyphrase:")
        code=gamestate.mainwin.get_line("\n> ")
        if str(code) == self.keyphrase:
            self._trip()
            gamestate.mainwin.put_line("Passphrase accepted!")
        else:
            gamestate.mainwin.put_line("Invalid passphrase.")
        gamestate.mainwin.update()
        gamestate.mainwin.wait()
        win.fgcolor=oldfg

    def draw(self,window):
        r, g, b = 0x2C, 0x90, 0x0B
        if random.randint(0, 5) == 0:
            g -= random.randint(0,25)
        super().draw(window, (r, g, b))
        self.glow((r, g, b),3,window,hard=True)

class NeutronBeam(GameObj):
    """A segment of a neutron beam. Kills players that cross it while allowing inorganic objects to pass through unharmed. If there isn't a
    ProtonBeam, ProtonBeamReflector, or active ProtonBeamTransmitter object to the left or right (if vertical==False) or up or down
    (if vertical==True), this object removes itself from the world."""

    def __init__(self,startpos,vertical):
        super().__init__(None,startpos)
        self.vertical=vertical
        if vertical:
            self.graphic='|'
            self.checkpositions=((0,1),(0,-1))
        else:
            self.graphic=" "
            self.checkpositions=((1,0),(-1,0))

    def interact(self, player, gamestate):
        player.kill(self, gamestate)

    def update(self, gamestate):
        pass

class ProtonBeamEmitter(GameObj):
    """Fires a ProtonBeam according to its angle attribute. Places ProtonBeam objects along a raycast, up to the first
    thing it hits - which may well be another ProtonBeam (so ProtonBeams can't cross). If the object the beam hits is a
    ProtonBeamReciever, that object is .trigger()-ed."""

    def __init__(self,startpos,angle=0,startactive=True):
        super().__init__('¥',startpos)
        #We keep track of our own beam objects 'cos we delete and replace them every turn
        self.active=startactive
        self.beamobjs=[]
        self.angle=angle

    def update(self,gamestate):
        #First we kill all our old beamobjs
        for thebeamobj in self.beamobjs:
            gamestate.current_level.remove_obj(thebeamobj)
        self.beamobjs.clear()

        if self.active:
            #Do a raycast to grab the position we're drawing the beam to
            dc=math.cos(math.radians(self.angle))
            ds=math.sin(math.radians(self.angle))
            direction = ((dc*0 - ds*-1),(ds*0 - dc*-1))
            x,y = self.pos
            positions=raycast((x,y+1),direction,gamestate.current_level,retlist=True)

            #Then we place ProtonBeam objects along the ray
            for thepos in [position for position in positions if not isinstance(gamestate.current_level.get_at(position),Wall)]:
                self.beamobjs.append(ProtonBeam(thepos))
            gamestate.current_level.safe_objlist.extend(self.beamobjs)

    def cleanup(self,gamestate):
        for thebeamobj in self.beamobjs:
            gamestate.current_level.remove_obj(thebeamobj)

    def trigger(self):
        self.active=not self.active


class ProtonBeam(GameObj):
    """A segment of a proton beam. This is created and destroyed by a ProtonBeamTransmitter object. It destroys any other objects it encounters."""

    def __init__(self,startpos):
        self.proton_safe=(Wall,Gate,ProtonBeamReceiver,ProtonBeamEmitter)+no_collide_objs
        super().__init__('⁓',startpos)

    def update(self, gamestate):
        occupant=gamestate.current_level.get_at(self.pos,exclude=(self,))
        if occupant and not isinstance(occupant,self.proton_safe):
            if isinstance(occupant,Missile):
                occupant.kill(self,gamestate)
            else:
                gamestate.current_level.remove_obj(occupant)
        #We kill the player in an unusual way because otherwise he slips through sometimes when there's other things on
        #the square with him.
        if gamestate.current_level.plyobj.pos==self.pos:
            gamestate.current_level.plyobj.kill(self,gamestate)


    def draw(self,window):
        mycolor=random.choice(['darkgoldenrod', 'darkgoldenrod1', 'darkgoldenrod2', 'darkgoldenrod3', 'darkgoldenrod4', 'goldenrod',
                               'goldenrod1', 'goldenrod2', 'goldenrod3', 'goldenrod4', 'lightgoldenrod', 'lightgoldenrod1', 'lightgoldenrod2',
                               'lightgoldenrod3', 'lightgoldenrod4', 'lightgoldenrodyellow', 'palegoldenrod'])
        window.terminal[self.pos].fgcolor=mycolor
        window.terminal[self.pos].character = self.graphic
        super().draw(window)
        self.glow(mycolor,2,window)

class ProtonBeamReceiver(Trigger):
    """A trigger that trips when a proton beam shines on it.."""

    def __init__(self,startpos,target,singleimpulse=False):
        super().__init__('◘',startpos,target)
        self.singleimpulse=singleimpulse
        self.tripped=False

    def update(self,gamestate):
        wastripped=self.tripped
        x,y=self.pos

        for xofst, yofst in ((-1,-1),(-1,1),(1,-1),(1,1)):
                neighbor = gamestate.current_level.get_at((x+xofst,y+yofst))
                if isinstance(neighbor,ProtonBeam):
                    self.tripped=True
                    break
        else:
            self.tripped=False

        if self.tripped != wastripped:
            if (self.singleimpulse and wastripped):
                return
            self._trip()

    def draw(self,window):
        if self.tripped:
            mycolor="palegoldenrod"
        else:
            mycolor="darkgoldenrod"
        super().draw(window,mycolor)

class TutorialDisplaySystem(GameObj):
    """The object responsible for displaying the tutorial on level 0."""

    def __init__(self):
        super().__init__('',(1,1))
        self.state="levelstarted"

    def __repr__(self):
        #self()
        return "Type tutorial() and press enter to begin the tutorial."

    def __call__(self):
        if self.state == "waitingforcall":
            print("Welcome to the INJECTION tutorial!")
            print("Before we begin, let me ask you something...")
            print("Do you already know how to code? (type y for yes or n for no, then press enter).")
            if input().lower().strip() in ("y", "yes"):
                print("Okay, great! Do you already know how to code _in Python 3_?")
                if input().lower().strip() in ("y","yes"):
                    print("Alright! In that case, all you need is a few tips:")
                    print("The 'injection console' brought up by pressing semicolon (;) is just the Python REPL.")
                    print("Calling dir() gives you a list of all the variables currently in scope.")
                    print("info() is a function that gives you an overview of any given object.")
                    print("You can call hint() to get... a hint. Call it again to get another hint.")
                    print("Aside from that you ought to be set!")
                else:
                    print("Okay! I'm bringing up a website with a Python 3 'crash course' of sorts.")
                    print("It's just a syntax guide; keep it open in the background while you work.")
                    webbrowser.open("http://learnxinyminutes.com/docs/python3/")
                    print("Here's a few tips specific to this game:")
                    print("The 'injection console' brought up by pressing semicolon (;) is just the Python REPL.")
                    print("Calling dir() gives you a list of all the variables currently in scope.")
                    print("info() is a function that gives you an overview of any given object.")
                    print("You can call hint() to get... a hint. Call it again to get another hint.")
                    print("Aside from that you ought to be, uh, set (get it? 'set'?).")
            else:
                print("Would you like to go through the interactive tutorial?")
                print("It'll teach you just enough programming to be able to play the game.")
                if input().lower().strip() in ("y","yes"):
                    print("""
Okay!
The first thing I'm going to teach you about is 'variables'.
In Python, a 'variable' is just a name for something.
You assign a variable (make the variable be a name for something)
with the = sign. The = sign makes the thing on the left of it a name
for whatever is on the right of it. For example, if I say:

myvariable = 7

... then I've just made myvariable a name for the number seven.
\nTry making the variable myvariable mean the number 12.
                    """)
                    while not self.interactive_test(lambda x: x["myvariable"]==12):
                        print("No, that's not it.")

                    print("Great!")

                    print("""
Now, that 12 you typed is what's called a "literal." It's called that because,
well, you literally typed 12. While a variable is a name for something, a
literal is a textual representation of something; just like 12 is a textual
representation of the number twelve.

There are other kinds of literals; the most important one is a string literal.
A string is just a bunch of letters (in programming letters are usually called
"characters"). "Bla bla" is a string literal representing a capital B, a
lowercase l, a lowercase a, a space, a lowercase b, et cetera.

You can think of the quotes as meaning "literally this text." The value
"bla bla" literally means bla bla.

Just like numbers, you can make a variable be a name for a string literal
as well.

Try making myvariable mean "I'm learning Python!"
                    """)
                    while not self.interactive_test(lambda x: x["myvariable"]=="I'm learning Python!"):
                        print("No, that's not it. Make sure you get the case exactly right; \"a\" is a totally different thing than \"A\"!")

                    print("Great!")

                    print("""
You can also do math on variables and literals. You do that about the way you'd
expect: + represents "add these numbers", - represents "subtract these
numbers". We use the * to represent multiplying, and / to represent dividing.
Like this:

>> 2+2
4

>> 2 * 4
8

>>two = 6 / 3
>>two
2

Try making the variable x equal 12 times 12.
                    """)
                    while not self.interactive_test(lambda x: x["x"]==144):
                        print("No, that's not it.")

                    print("""
There's one more type of literal; the two Boolean literals, True and False.
Those are easier to figure out on your own by playing the game, so I'll
just let you do that.

We're one-third done here. The next thing you need to know about are sequences.
There are three primary types of sequences; strings, lists and tuples. You've
seen strings already, so we'll talk about lists now instead. A list literal
looks like this:

["a", "b", "c"]

... and a tuple literal looks like this:

("a", "b", "c")

They look very similar, and they are! The only difference is that tuples
can't be modified; you can make the name for the tuple mean a different
tuple, but only with lists can you stick things on the end of the same list
or take stuff out.

You can refer to the stuff in a sequence by putting a number in square brackets
( [ and ] ) after it. So you can do this:

>> mylist
[2, 4, 6, 8]

>> mylist[0]
2

>> mylist [1]
4

>> mylist [3]
8

There's a list in the console in every level in INJECTION called objs. objs
is the list of all the objects in the level that you can use the console to
interact with. Try it once you get out of the tutorial.

And remember, = makes the name on the left a name for whatever is on the right.
So, you can do stuff like this:

>> a=[1, 2]
>> b=a
>> b
[1, 2]

>> b[1]=3
>> b
[1, 3]

>> a
[1, 3]

Now, make the variable mylist equal a list of 1, 2, and 3.
                    """)

                    while not self.interactive_test(lambda x: x["mylist"]==[1, 2, 3]):
                        print("No, that's not it.")

                    print("""
Only two more things you need to know about and then we're done.

Everything we've talked about so far is an "object". 1 is an integer object.
"Bla" is a string object. [1, 2, 3] is a list object with three integer
objects in it. Everything you can do anything with in Python is an object.

Some objects don't have textual representations, and so when Python tries
to print them out you'll see something like this:

<gameobjs.Level1Keypad object at (10,10)>

This actually has a lot of complex meaning, but you'd be bored by the time
I explained it all to you. Instead, the only part you care about right now
is the "Level1Keypad" part; that's usually a short description of what kind
of object it is. So if it's a "Gate", and you see something that opens and
closes somewhere in the level, they're probably one and the same.

The second-from-last thing you need to know about is attributes.

Attributes are variables that are attached to objects. An attribute is a name
for something, just like a variable. Let's say myobj is a name for some
object, and that object has an attribute pos. Myobj's pos attribute is the
name for a tuple that represents myobj's position in the level in cartesian
coordinates. I can talk about myobj's pos by putting a . after myobj, and pos
after the dot. Like this:

>> myobj.pos
(10, 10)

Just like variables, you can use = to assign attributes:

>> myobj.pos = (12,34)
>> myobj.pos
(12, 34)

Try it: I've made an object called myobj. Set its coolness attribute to "maximum".
                    """)

                    class GenericObject:
                        pass

                    while not self.interactive_test(lambda x: x["myobj"].coolness=="maximum",{"myobj":GenericObject}):
                        print("No, that's not it. You want myobj.coolness to equal \"maximum\".")

                    print("""
The last thing I'm going to teach you about is functions.

A function is something you can tell the computer to do. You "call" a function
by saying the function's name (which is a variable or attribute like anything
else), followed by (). So if I have a function myfunction, I can call it like:

>> myfunction()

... and then whatever myfunction does, that will happen.

There are a couple functions that are really handy for solving the puzzles
in this game. dir() is a function that returns a list of all the variables
currently available in the console. Use it like this:

>> dir()
['__builtins__', 'hint', 'info', 'objs', 'options', 'tutorial']

... and that tells you that there's things named "hint", "info", "tutorial",
etc... that you can talk about in the console.

info() is another function that's always available in the console. info()
takes a parameter; you have to give it something to do what it does to. You
can give a function a parameter, assuming it actually wants a parameter, by
putting the name of the thing you want to put in to the function inside the
parentheses. To call info() on the object referred to as myobj, you type this:

>> info(myobj)

That will show you all the attributes and methods (a function that is referred
to by an attribute is called a method) on myobj.

Some functions and methods take multiple parameters; separate them with commas,
like this:

>> myfunc(a, b)

There's also such a thing as keyword arguments. info() takes the keyword
argument showspecial. You can pass keyword arguments to functions like this:

>> info(myobj,showspecial=True)

Try calling the function myfunc() on the string "param1" for the first
parameter and the number 2 for the second parameter.
                    """)

                    callsuccessful=[False]
                    def myfunc(param1, param2):
                        if param1=="param1" and param2==2:
                            callsuccessful[0]=True

                    while not self.interactive_test(lambda x: callsuccessful[0],{"myfunc":myfunc}):
                        print("No, that's not it. Separate the arguments with commas.")

                    print("""
Alright, tutorial done!

You've actually only learned enough to get to level seven or so. You'll
be able to figure out a lot of stuff on your own and with the information
info() gives you, but I highly recommend looking up an actual Python tutorial
if you enjoy the game and want to get the most out of it.

Oh, one last thing. There's a function called hint that takes no parameters.
Call it like this:

>> hint()

... to get a hint on how to solve the level. Call it again and it'll give
another hint.

And THAT's tne end of the tutorial. Have fun!
                    """)

        return

    def interactive_test(self,evaluator,elocals={}):
        """Give the player a chance to try something he just learned in the interactive Python tutorial. evaluator
        is a callable that takes a dict that represents the locals of the environment after the player's input was
        evaluated; the return value of this is returned. Elocals is a dict that becomes the initial locals of the
        exec environment."""
        try:
            exec(input(">> "),{},elocals)
            return evaluator(elocals)
        except Exception:
            print(traceback.format_exception_only(*sys.exc_info()[:2])[-1])
            return False

    def update(self,gamestate):
        if self.state == "levelstarted":
            print("Type 'tutorial()' and press ENTER to begin the tutorial.\n")
            self.state="waitingforcall"
        elif self.state == "waitingforcall":
            pass
        elif self.state == "":
            pass

    def draw(self,window):
        if self.state == "waitingforcall":
            window.put_line_at("Press ; to bring up the injection console", (0, 0))
            window.put_line_at("Then type tutorial() to continue the tutorial", (0, 1))

class AttributeCycler:
    """An object that, when triggered, cycles a given object's given attribute through a list of given values.
    I.e. if I have a GravPad object named gp, I can create an AttributeCycler(gp,"gravity_direction",[(-1,0),(1,0)]) that,
    when triggered by some kind of trigger (such as a pressureplate) will cause the GravPad to pull towards (-1,0),
    then after being triggered again, (1,0), then after another triggering back to (-1,0). The value the attribute
    is initialized with remains until the first time the cycler is triggered, but after that is forgotten. You should
    therefore probably set the initial value to be the last value in cycler list."""

    def __init__(self,subject,subjectattrib,valuelist):
        self.subject=subject
        self.subjectattrib=subjectattrib
        self.valuelist=valuelist

    def trigger(self):
        #Pull the first element off the list...
        newvalue=self.valuelist.pop(0)
        #set the subject's attribute to it
        setattr(self.subject,self.subjectattrib,newvalue)
        #then stick the value back on the end of the list
        self.valuelist.append(newvalue)

class SignalRetriggerDelay(Trigger):
    """An immaterial trigger that, when triggered, will trigger its target, wait delay frames, then trigger its target again."""

    def __init__(self,target,delay):
        super().__init__("",(-1,-1),target)
        self.delay=delay
        self.countdown=0

    def trigger(self):
        self.countdown=self.delay
        self._trip()

    def update(self,gamestate):
        if self.countdown:
            self.countdown-=1
            if self.countdown==0:
                self._trip()
                
class ArbitraryCodeExecutor(GameObj):
    """Given an arbitrary string of Python code, runs it with exec() once every tick. Executes the code with
    the locals dict given to its constructor; if no locals is given, uses the instantiation-time locals.
    Remember: .code is a STRING containing code, not a function!"""

    def __init__(self,startpos,code,execlocals=None):
        super().__init__("␍",startpos)
        self.code=code
        self.errored=False
        if execlocals is None:
            self.execlocals=locals()
        else:
            if "self" not in execlocals:
                execlocals["self"]=self
            self.execlocals=execlocals

#    def special_locals(self):
#        """Since exec() sticks a bunch of random crap in .execlocals, this function returns a string listing only the
#        ones that are specific to this ACE. You should probably print() its return value so it looks nice."""
#        retstr=""
#        thingsthatgetstuffedinlocals=globals()
#        for key, value in self.execlocals.items():
#            if key not in thingsthatgetstuffedinlocals:
#                retstr+="{0} : {1}\n".format(key,value)
#        return retstr


    def update(self,gamestate):
        try:
            main.call_with_timeout(exec,(1/30),self.code,globals(),self.execlocals)
        except Exception:
            self.errored=True
            raise
        else:
            self.errored=False

    def draw(self,window):
        if self.errored:
            color="red"
        else:
            color=None

        super().draw(window,color)

    def __getstate__(self):
        mydict=self.__dict__.copy()
        for k, v in self.__dict__.items():
            if inspect.ismodule(v):
                del mydict[k]
        return mydict

class HoloProjector(GameObj):
    """A gameobj that allows the player to draw on the screen. Use its .put_color(pos,color) method to put color on the screen,
    and use .clear() to clear it."""

    def __init__(self,startpos):
        super().__init__("",startpos)
        self.pixels=dict()

    def put_color(self,pos,color):
        """Put a block of color at the given coordinates. Pos is a 2-tuple representing the position to put the color at.
        Color is 3-tuple representing the color itself in RGB format. I.e. (255,0,0) is red, (0,255,0) is green, (0,0,255) is blue.
        It'll also accept Pygame color words (i.e. 'red' 'green' 'blue' 'yellow')."""
        if not (isinstance(pos,tuple) and len(pos) == 2):
            raise ValueError("Invalid pos: should be a tuple of two ints")

        try:
            if isinstance(color,tuple):
                self.pixels[pos]=pygame.Color(*color)
            else:
                self.pixels[pos]=pygame.Color(color)
        except ValueError:
            raise ValueError("Invalid color: should be a tuple of three ints or valid color name string")

    def clear(self):
        """Remove all the color projections this projector is responsible for."""
        self.pixels.clear()

    def valid_color_names(self):
        """Return a list of strings that is all the valid string color names you can use. Warning: this is a big list!"""
        import pygame.colordict
        return list(pygame.colordict.THECOLORS.keys())

    def draw(self,window):
        #The projector's color is the average of all the colors it's projecting
        if self.pixels:
            rs=[]
            gs=[]
            bs=[]
            for thecolor in self.pixels.values():
                rs.append(thecolor.r)
                gs.append(thecolor.g)
                bs.append(thecolor.b)
            r = sum(rs)//len(rs)
            g = sum(gs)//len(gs)
            b = sum(bs)//len(bs)
            mycolor=(r,g,b)
        else: #if we have no pixels, default to gray
            mycolor=(127,127,127)

        super().draw(window,mycolor)
        #we also glow with our color with intensity proportional to the nubmer of pixels we're projecting
        self.glow(mycolor,len(self.pixels.values())//3,window)

        #oh, and draw the pixels.
        for pos, color in self.pixels.items():
            window.paint(*pos,bgcolor=color)


class SimpleAIActor(GameObj):
    """Base class for simple value-maximizing AI driven gameobjs. Walks a decision tree and then takes the action that
    leads to the best outcome. Outcomes are analyzed by the evaluate() method, which takes a Level object and determines
    how 'good' it is. The more desirable the game state, the higher a value evaluate() returns. For example, an AI object
    that wants to keep the player from completing the level might have an evaluate() that returns a lower score the closer
    the player is to the levelporter. The decision making process also adds a score penalty for the number of steps needed
    to get to that node on the tree, so if there's two sets of decisions that lead to the same outcome the shorter sequence
    will be prioritized over the longer. This doesn't take into account the player; it always assumes he'll just sit there
    doing nothing.

    The get_actions() method returns a list of all possible actions the actor can take this turn.
    The perform_action() method actually performs a given action. Actions can be any kind of object.
    The reverse_action() method un-does a given action. It should raise ValueError if it doesn't know how.
    """

    def __init__(self,startpos,graphic,maxdepth=4):
        super().__init__(startpos,graphic)
        self.maxdepth=maxdepth
        self.evaluate=lambda x: 0
        self.amclone=False #am I a clone (if true) or am I the real deal / existing in the actual game level (if false)?

    def clonelevel(self,level):
        """Create a "simulation" copy of the actual playing level, so that we can use it to predict what results our
        actions will have."""
        objlist=copy.deepcopy(level.objlist)
        safeobjlist=copy.deepcopy(level.objlist)

        if self in objlist:
            objlist.remove(self)
        if self in safeobjlist:
            safeobjlist.remove(self)

        return levels.Level(objlist,safeobjlist,Player())

    def cloneself(self):
        """Create a "remote control" clone of ourself to go into the "simulation" world copies."""
        fakeself=type(self)(self.pos,'')
        fakeself.amclone=True
        #fakeself.evaluate=self.evaluate #we just use our own evaluate...
        return fakeself

    def update(self,gamestate):
        #Every time the REAL level calls our update(), we clone the level, look into the future to see what we should do
        #and then do it.
        if self.amclone:
            #Clones shouldn't respond to update() because the AI they're cloned from is telling them what to do
            return
        action, value=self.simulate(self.clonelevel(gamestate.level))
        self.perform_action(action)

    def simulate(self,level,depth=0):
        """Given a 'simulation' level, create a 'remote control' duplicate of ourself and consider what consequences its
         actions might have. We then return the action that led to the best result (the highest value when the world was
          passed to evaluate().)"""

        actions=self.get_actions(level)

        #If we have no options, OR we've been thinking too long, we terminate the search
        #the world state we got up to in our thinking ahead will be the only one we actually create a numerical value for
        #we'll then proceed down the path that led to the best result at the end of the chain of actions
        #i.e. we consider moving left and moving right but we don't care what moving left or right immediately do
        #instead we look at what moving left and moving right after having moved left and right do
        #and we keep doing that until we've hit our depth or we can't proceed any further.
        #for example, if moving left then left takes us into a laser that destroys us, we don't care about the first left,
        #we only care about the end result (to the extent that we can actually look ahead).
        if depth > self.maxdepth or not actions:
            return (None,self.evaluate(level))

        results=[]
        for theaction in actions:
            levelclone=self.clonelevel(level)
            fakeself=self.cloneself() #We create a fresh level and clone each time to account for the fact that things will change
            levelclone.safe_objlist.append(fakeself)

            fakeself.perform_action(theaction,levelclone)

            for theobj in levelclone.objlist+levelclone.safe_objlist:
                try:
                    theobj.update()
                except Exception:
                    pass #@TODO: Do a smart thing here

            resulttuple=fakeself.simulate(levelclone,depth+1)
            results.append(resulttuple)

        result = max(results.items(),key=lambda x: x[1])
        return result

    def get_actions(self,level):
        return []

    def perform_action(self,level,action):
        return

    def reverse_action(self,level,action):
        raise NotImplementedError
        return

class TestAIActor(SimpleAIActor):
    """An AI actor designed to test the AI actor system."""

    def get_actions(self,level):
        x,y=self.pos
        actions=[]
        for rpos, actionstr in (((-1,0),"left"),((1,0),"right"),((0,-1),"up"),((0,1),"down")):
            rx, ry = rpos
            if not movement_check((x+rx,y+ry),level,self):
                pass

    def perform_action(self,level, action):
        x,y = {"left":(-1,0), "right":(1,0), "up":(0,-1), "down":(0,1)}[action]


class NonPlayerCharacter(GameObj):
    """A GameObj that is supposed to be a person. This class can also be instantiated directly to represent generic NPCs.

    You know the old saying that any sufficiently complex program contains a partial implementation of Lisp? Well..."""

    def __init__(self,graphic,startpos,color=None):
        super().__init__(graphic,startpos)
        self.actionqueue=[]
        self.stack=[]
        self.currentspeech=""
        self.speechcleartime=0
        if color:
            self.color=color
        else:
            self.color=(127,127,127)
        self.waittimer=0
        self.waitcondition=None
        self.interrupts={}

    def update(self,gamestate):

        #first process interrupts
        toremove=[]
        for key, theinterrupt in self.interrupts.items():
            condition,actionlist,repeat = theinterrupt
            if condition():
                self.stack.append((self.waittimer,self.waitcondition,self.actionqueue.copy()))
                self.waittimer=0
                self.waitcondition=None
                self.actionqueue.clear()
                self.queue_many_actions(actionlist)
                #We remove repeat interrupts too - they'll trip continuously and cause badness
                toremove.append(key)
                #instead we just append a "re-set this interrupt" command at the end.
                if repeat:
                    self.queue_action(self,self.set_interrupt,key,condition,actionlist,repeat)
        for key in toremove:
            del self.interrupts[key]


        #if we've got a delay before acting again...
        if self.waittimer:
            self.waittimer-=1
        #if a statement has to be true before acting again...
        elif self.waitcondition is not None:
            if self.waitcondition():
                self.waitcondition=None
        #otherwise pop another action off the queue and execute it
        else:
            if self.actionqueue:
                action=self.actionqueue.pop()
                action()

    def get_bg_color(self):
        """Return a color that our .color would stand out against, or pure white if main._high_contrast is True."""
        #'invert' the color by taking its RGB values and subtracting them from 255; i.e. 128 becomes 128, 255 becomes 0,
        #64 becomes, uh, 255-64. You get the picture.
        if main._high_contrast: return (255,255,255)

        #print(self.color, file=sys.__stdout__)
        self.color = pc._color_name_conversion(self.color)
        inversecolor=pygame.Color(*((255-self.color[0])//2,(255-self.color[1])//2,(255-self.color[2])//2))
        h,s,l,a=inversecolor.hsla
        l=l/2 #Halve luminosity (brightness)...
        s=s/1.5 #and reduce saturation (color intensity) by 25%.
        inversecolor.hsla=(h,s,l,a)
        return tuple(inversecolor)

    def draw(self,window):
        super().draw(window,self.color)
        if self.currentspeech:
            if self.speechcleartime <= 0:
                self.currentspeech=""
            else:
                self.speechcleartime-=1

                x=self.pos[0]-len(self.currentspeech)//2
                x=clamp(x,minv=0,maxv=(80-len(self.currentspeech)))
                y=self.pos[1]-1

                if main._high_contrast:
                    fgcolor=(0,0,0)
                else:
                    fgcolor=self.color
                bgcolor=self.get_bg_color()

                textpos = pc.Pos(x,y).clamp(max = window.terminal.size)

                window.put_line_at(self.currentspeech,textpos,fgcolor=fgcolor,bgcolor=bgcolor, allow_wrap=False)

    def move(self,dir,level):
        """Make this character move accroding to the given direction vector."""
        newpos=(self.pos[0]+dir[0],self.pos[1]+dir[1])
        if not movement_check(newpos,level,self):
            self.pos=newpos

    def say(self,text,secondstoshow=3,autowait=True):
        """Make the character say the given text. It'll stay around for the given number of seconds."""
        self.currentspeech=text
        self.speechcleartime=round(secondstoshow*15)
        if autowait:
            self.wait(round(secondstoshow))

    def wait(self,waittime):
        """Stop processing the action queue for waittime many seconds."""
        self.waittimer+=round(waittime*15)

    def wait_trigger(self,condition):
        """Given a callable, waits and calls the callable repeatedly until it returns True. If the wait() timer
        is running it runs down first before the NPC begins checking the trigger."""
        self.waitcondition=condition

    def queue_action(self,method,*params):
        """Put this action onto this NPC's action queue. Actions will be taken once per game tick. The method param
        is a method of the NPC that will be called; put the parameters you wish passed to it after it."""
        if not callable(method):
            raise ValueError("{0} is not callable".format(method))
        partialobj=functools.partial(method,*params)
        self.actionqueue.insert(0,partialobj)

    def queue_many_actions(self,actionlist):
        """Given a list of tuples of (method,param1,param2,...), call self.queue_action(*actionlist[n]) for every tuple in actionlist."""
        for thetuple in actionlist:
            #commands like (mynpc.pop_stack) for example will not create a tuple - it evals to just mynpc.pop_stack. This accounts for that.
            if inspect.isroutine(thetuple):
                self.queue_action(thetuple)
            else:
                self.queue_action(*thetuple)

    def pop_stack(self):
        """Return fron an interrupt routine. See the docstring for set_interrupt."""
        timerstate, conditionstate, actionq = self.stack.pop()
        self.waittimer=timerstate
        self.waitcondition=conditionstate
        self.actionqueue.extend(actionq)

    def set_interrupt(self,name,interruptcondition,actionlist,repeat=False):
        """Basically, every update tick, evaluate interruptcondition. If it is true, queue_many_actions(actionlist).
        That's not actually how it works at all, though - when an interrupt trips, it pushes the current action queue
        (i.e. a list of all actions that haven't been exectured yet) onto the "stack". An interrupt routine can then
        end with pop_stack(), which pops the last queue off the stack and appends it to the current queue. Of course
        you can have multiple partially-completed queues on the stack at a time, such as if an interrupt itself gets
        interrupted.

        If multiple interrupt conditions are true, they will execute in reverse insertion order (assuming they all
        pop_stack(); otherwise processing will end there.)

        If repeat is False, as it is by default, the interrupt is "cleared" and forgotten after it trips;
        if it is true it is not.

        You can implement 'cascading' interrupts (i.e. the first time the interrupt triggers actionlist a is executed,
        the second time actionlist b is executed) by having a set_interrupt at the end of your interrupt actionlist
        right before the pop_stack().
        """
        self.interrupts[name]=(interruptcondition,actionlist,repeat)

    def unset_interrupt(self,name):
        """Disable the given interrupt. Returns True if it existed, False otherwise."""
        try:
            del self.interrupts[name]
            return True
        except KeyError:
            return False

    def unset_all_interrupts(self):
        """Disable ALL interrupts."""
        self.interrupts.clear()

class Water(GameObj):
    """A GameObj that represents liquid water.

    Every tick the water object looks at all adjacent tiles. Adjacencies that are considered blocked for movement
    purposes and do not contain a water object are ignored. If an adjacency is empty, a new water object is created
    there. If an adjacency contains a water object (or a new water object was just created there) the water object
    proceeds with the flow simulation.

    A water object has a 'level' value in (0,10] (0 < flow <= 10) and a 'flow' value that is a 2D direction vector.
    Each turn each water object takes the direction vector to each of its neighbors and scales it by the difference
    between its level and the adjacent object's level. These vectors are all then summed to the object's flow vector.
    The flow vector is NOT reset between computations; the idea is to give an impression of force (from higher water to
    lower) and momentum (since the flow does not immediately change)

    Then, for each adjacent water object whose level is lower than this object's, it calculates a transfer value by
    multiplying the difference between the level values with the dot product of the direction vector to that object
    and this object's flow vector. If the dot product turns out to be negative the computation stops.
    Thus waters that are 'aligned' with the flow, blocks that the flow vector is pointing towards, will be assigned
    a higher transfer value than water objects that are perpindicular to the flow vector. Water objects that the flow
    vector is actually pointing away from will just be ignored (they'll transfer their water later). The transfer value
    is then subtracted from this object's level and added to the other object's level. The other water's flow vector
    will then be summed with this object's flow vector scaled by the transfer value."""

    def __init__(self,startpos,level=10,flow=(0,0)):
        super().__init__('~',startpos)
        self.level=level
        self.flow=flow
        self.evaptimeout=0

    def update(self,gamestate):

        if self.level < 0.1:
            self.evaptimeout+=1
            if self.evaptimeout >= 15:
                gamestate.current_level.remove_obj(self)
        else:
            self.evaptimeout=0

        mypos=Vector2(self.pos)
        adjacentpositions=(mypos + (Vector2(dx, dy)) for dx in (-1, 0, 1) for dy in (-1, 0, 1) if (dx, dy) != (0,0))

        if math.isnan(self.level):
            raise ValueError("Water level is NaN. !?!?")

        adjacentwaters=[]
        for thepos in adjacentpositions:
            hit=movement_check(thepos,gamestate.current_level,self)
            if hit is None:
                #If the space is empty, put water there
                #@TODO: check if this doesn't cause repeated spawning of water blocks cos get_at doesn't give back the new water object if another object is already there
                existingobject=gamestate.current_level.get_at(thepos)
                if isinstance(existingobject,Water):
                    adjacentwaters.append(existingobject)
                else:
                    if self.level > 0.1:
                        newwater=Water(thepos,0)
                        gamestate.current_level.safe_objlist.insert(0,newwater)
                        adjacentwaters.append(newwater)
            elif isinstance(hit,Water):
                adjacentwaters.append(hit)

        lowerwaters=[]
        for thewater in adjacentwaters:
            if self.level > thewater.level:
                lowerwaters.append(thewater)
            self.flow += (Vector2(thewater.pos) - mypos) * (self.level - thewater.level)

        #The * operator takes dot product of vectors
        lowerwaters.sort(key=lambda x: self.flow*x.flow)

        originallevel=self.level
        for thewater in lowerwaters:
            transfervalue=(originallevel - thewater.level) / 2 #* self.flow * (Vector2(thewater.pos) - mypos)) / 10

            print(transfervalue,file=sys.__stdout__)

            if transfervalue <= 0:
                continue
            if transfervalue > self.level:
                transfervalue=self.level

            thewater.level+=transfervalue
            self.level-=transfervalue
#            thewater.flow+=(self.flow * transfervalue)
#            self.flow-=(self.flow * transfervalue)

            if self.level==0:
                break

        try:
            self.flow=self.flow.normalize()
        except ValueError:
            pass

        #self.flow-=(self.flow)

    def draw(self,window):
        window.putchar(str(clamp(round((self.level)),9,0)),*self.int_pos(),fgcolor="blue",bgcolor="darkblue")

class Radar(GameObj):
    """A GameObj that detects other GameObj-s of a given class in the level. Its .range parameter specifies how close
    objects have to be before it can detect them. Its .lineofsight parameter specifies whether or not it has to have
    line-of-sight to the object before it can detect it. Detected objects are stored in its """

class ConditionalObstruction(GameObj):
    """A GameObj that only appears solid to other GameObj-s that fit a given criterion.
    That criterion is specified by a given callable, be_solid_if, which returns
    True if the object should be collided with.

    For example (assuming this is objs[0])...

    def let_me_through(theobj):
     return theobj.graphic != '@'
    objs[0].be_solid_if=let_me_through

    ... will cause the ConditionalObstruction to be solid to everything but the player.
     """

    def __init__(self,startpos,be_solid_if=lambda x: True):
        super().__init__('␡',startpos)
        self.be_solid_if=be_solid_if

    def collide(self,other,nonphysical=False):
        if nonphysical:
            return True

        if other is None:
            return True

        return self.be_solid_if(other)

class TriggerFunction(GameObj):
    """Executes the given function when triggered."""

    def __init__(self, startpos, func):
        super().__init__('', startpos)
        self.showtrippedtimer=0
        self.func=func

    def trigger(self):
        self.func()
        self.showtrippedtimer=8

    def draw(self,window):
        color = "skyblue" if self.showtrippedtimer else None
        super().draw(window, color)
        if self.showtrippedtimer: self.showtrippedtimer-=1

class StasisField(GameObj):
    """Makes everything around it update on every strength-th frame. I.e. if strength is 4, things in the field miss
    three .update() calls for every one they get."""

    def __init__(self, startpos, range, strength):
        super().__init__('Å', startpos)

        self.range = range
        self.strength = strength
        self.subjects = set()
        self.oldupdatefuncs={}
        self.counter = 0
        self.updateallowed = True

    #@TODO: If something _STARTS_ in the effect range of the stasis field generator, it won't get a single update
    #until it leaves the field. I'm sure I'll realize what the problem is as soon as I look at this without being exhausted.
    #UPDATE: I didn't.
    def update(self,gamestate):

        if self.counter >= self.strength:
            self.counter = 0
            self.updateallowed = True
        else:
            self.counter += 1
            self.updateallowed = False

        currentsubjects = set()
        x, y = self.pos
        for dx, dy in self.relativeposestocheck:
            theobj = gamestate.current_level.get_at((x + dx, y + dy), True)
            if theobj and not isinstance(theobj, Wall):
                currentsubjects.add(theobj)

        newsubjects = currentsubjects - self.subjects
        formersubjects = self.subjects - currentsubjects
        if formersubjects: print(formersubjects, file=sys.__stdout__)
        self.subjects.update(currentsubjects)
        self.subjects.difference_update(formersubjects)

        for thesubj in newsubjects:
            oldupdate = thesubj.update
            self.oldupdatefuncs[thesubj] = oldupdate

            def wrappedupdate(gstate):
                if self.updateallowed:
                    #print("{0} updated".format(thesubj), file=sys.__stdout__)
                    oldupdate(gstate)
                #else:
                    #print("{0} blocked from updating".format(thesubj), file=sys.__stdout__)

            thesubj.update = wrappedupdate
            print("Caught {0}".format(thesubj), file=sys.__stdout__)

        for thesubj in formersubjects:
            thesubj.update = self.oldupdatefuncs[thesubj]
            print("Releasing {0}".format(thesubj), file=sys.__stdout__)

    @property
    def range(self):
        return self._range

    @range.setter
    def range(self, newrange):
        self._range = newrange
        rsqr = newrange * newrange
        self.relativeposestocheck = []
        for x in range(-100,100):
            for y in range(-100,100):
                if (x**2 + y ** 2) < rsqr and not x==y==0:
                    self.relativeposestocheck.append((x, y))

class Sign(GameObj):
    """A box with text in it.

    For normal gameobj purposes, a Sign's .pos is its center, biased towards the top left if it has even-numbered
    dimensions. It also has a .dimensions attribute which is a Pygame rect detailing its area and position. .pos
    is just an alias for .dimensions.center.

    The dimensions are the dimensions of the border, so a 3x3 sign has only one cell inside it for text.

    borderstyle can be "single", "double", "plusminus", or a a string like this: "╔═╗║╝╚"
    """
    builtinstyles = {
        "single"    : "┌─┐│┘└",
        "double"    : "╔═╗║╝╚",
        "plusminus" : "+-+|++",
    }

    def __init__(self, pos, text, dimensions=None,
                 textfgcolor=None, textbgcolor=None,
                 borderfgcolor=None, borderbgcolor=None, borderstyle="double"):
        if dimensions is not None:
            self.dimensions = dimensions
        else:
            #@TODO: Once pyconsolegraphics is smart enough to identify double-width characters, ask it for the len
            self.dimensions = pygame.Rect((0,0), (len(text), 1))
            self.dimensions.inflate_ip(2,2)
        self.text = text
        self.pos = pos
        self.textfgcolor   = textfgcolor
        self.textbgcolor   = textbgcolor
        self.borderfgcolor = borderfgcolor
        self.borderbgcolor = borderbgcolor
        self.borderstyle   = borderstyle

    @property
    def pos(self):
        return self.dimensions.center
    @pos.setter
    def pos(self, x):
        self.dimensions.center = x

    @property
    def borderstyle(self):
        return self._externalborderstyle
    @borderstyle.setter
    def borderstyle(self, styleorname):
        style = self.builtinstyles.get(styleorname, styleorname)
        if len(style) != 6:
            raise ValueError(f"Invalid borderstyle {style}")
        self._topleft, self._topbottom, self._topright, self._leftright, self._bottomright, self._bottomleft =\
            style
        self._externalborderstyle = styleorname

    def draw(self,window,color=None):
        widthminus2 = self.dimensions.width - 2
        top    = f"{self._topleft}{self._topbottom * widthminus2}{self._topright}"
        side   = f"{self._leftright}{' ' * widthminus2}{self._leftright}"
        bottom = f"{self._bottomleft}{self._topbottom * widthminus2}{self._bottomright}"
        window.center_line_at(top, self.dimensions.midtop, fgcolor=self.borderfgcolor, bgcolor=self.borderbgcolor)
        for t in range(-self.dimensions.height // 2 + 1, self.dimensions.height // 2 - 1):
            window.center_line_at(side, self.dimensions.center, (0, t+1),
                                  fgcolor=self.borderfgcolor, bgcolor=self.borderbgcolor)
        window.center_line_at(bottom, self.dimensions.midbottom, (0, -1),
                              fgcolor=self.borderfgcolor, bgcolor=self.borderbgcolor)

        paddedtext = self.text.format(f"^{widthminus2}")
        window.center_line_at(paddedtext, self.dimensions.center, fgcolor=self.textfgcolor, bgcolor=self.textbgcolor)

#Objects of these classes are considered passable; object such as boulders and missiles can move through them.
no_collide_objs=(PressurePlate,SafePressurePlate,GravPad,Teleporter,Skull,NeutronBeam,ProtonBeam,MissileTrail)
#Objects of these classes block lasers even if they're in no_collide_objs; for smoke (MissileTrail) mostly
block_laser_anyway_objs=(MissileTrail,)
#Objects of these classes are disposable - they're never considered "important" as far as the "press R to reset" hint is concerned
incidental_objs=(Missile,MissileTrail,NeutronBeam,ProtonBeam,Skull,LaserBeam,Water)
#Objects of these classes don't touch the floor and thus don't trip pressureplates
no_floor_contact_objs=(Missile,MissileTrail,NeutronBeam,ProtonBeam)
